import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreacionDocumentosRoutingModule } from './creacion-documentos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CreacionDocumentosRoutingModule
  ]
})
export class CreacionDocumentosModule { }
