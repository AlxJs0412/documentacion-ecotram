import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComunicadosRoutingModule } from './comunicados-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComunicadosRoutingModule
  ]
})
export class ComunicadosModule { }
