import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultasAvanzadasRoutingModule } from './consultas-avanzadas-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ConsultasAvanzadasRoutingModule
  ]
})
export class ConsultasAvanzadasModule { }
