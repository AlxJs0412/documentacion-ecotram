import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirma-dialog',
  templateUrl: './confirma-dialog.component.html',
  styleUrls: ['./confirma-dialog.component.scss']
})
export class ConfirmaDialogComponent {
  nrotramite: string;
  nrocopia:string;
  asunto:string;
  constructor(@Inject (MAT_DIALOG_DATA) public data: any) {
    this.nrotramite = data.nrotramite;
    this.nrocopia = data.nrocopia;
    this.asunto = data.asunto;
  }
}
