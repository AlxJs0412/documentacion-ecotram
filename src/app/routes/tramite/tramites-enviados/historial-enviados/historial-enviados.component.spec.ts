import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialEnviadosComponent } from './historial-enviados.component';

describe('HistorialEnviadosComponent', () => {
  let component: HistorialEnviadosComponent;
  let fixture: ComponentFixture<HistorialEnviadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialEnviadosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistorialEnviadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
