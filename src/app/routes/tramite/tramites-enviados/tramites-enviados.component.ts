import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {HistorialEnviadosComponent } from './historial-enviados/historial-enviados.component';
import { TramiteEnviadoService } from 'app/services/tramite-enviado.service';
import { TramiteModule } from '../tramite.module';
import { ConfirmaDialogComponent } from './confirma-dialog/confirma-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HistorialComponent } from '../tramites-recibidos/historial/historial.component';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';

interface Food {
  value: string;
  viewValue: string;
}

export interface tramite {
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}

@Component({
  selector: 'app-tramites-enviados',
  templateUrl: './tramites-enviados.component.html',
  styleUrls: ['./tramites-enviados.component.scss']
})
export class TramitesEnviadosComponent {
  lisTramite: tramite[] = [];
  //   {nro_tramite:273,nro_copia:"ORIGINAL",asunto: "PRUEBA - REMISION DE FACTURAS",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2023-04-12 11:20:43",tomado_por:"",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Urgente"},
  //   {nro_tramite:125,nro_copia:"ORIGINAL",asunto: "PLAN DE ORDENAMIENTO VEHICULAR",tipo_tramite:"ORDEN DE DESPACHO",procedencia:"INTERNO",fecha_envio:"2023-04-06 12:45:08",tomado_por:"maritzabel.rosso",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:""},
  //   {nro_tramite:145,nro_copia:"ORIGINAL",asunto: "ASTURIZAGA RODAS RAFAEL - BAJA DE VEHICULO",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"	2022-10-21 17:50:56",tomado_por:"lizeth.marin",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Baja"},
  //   {nro_tramite:100,nro_copia:"1",asunto: "TICONA MARZA MARIANA - ALTA PARA PAGO DE PATENTE Y CAMBIO DE NOMBRE",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2023-03-23 15:22:39",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Ata"},
  //   {nro_tramite:450,nro_copia:"ORIGINAL",asunto: "	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2022-03-04 09:09:56",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Media"},
  //   {nro_tramite:450,nro_copia:"ORIGINAL",asunto: "	ENBVIO DE PRUEBA - SOLICITUD",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2022-03-05 09:09:56",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Media"}
  // ];

  datos: any;
  //displayedColumns: string[] = ['acciones','nro_tramite','nro_copia','asunto','tipo_tramite','procedencia','fecha_creacion','fecha_modificacion','tomado_por','so_sistema_nombre','dias_atencion','prioridad'];
  displayedColumns: string[] = ['select','nro_tramite','nro_copia','cite','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad','opciones'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // dataSource: MatTableDataSource < any > = new MatTableDataSource < any > ([]);
  dataSource=new  MatTableDataSource(this.lisTramite);
  @ViewChild(MatSort) sort = new MatSort();
  selection = new SelectionModel<tramite>(true, []);
  ////combo
  selectedValue: string | undefined;

  accionesBloque: Food[] = [
    {value: 'recibir', viewValue: 'Recibir todos'},
  ];

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
  }
  }
  constructor (private svRecibidos: TramiteRecibidoService,private router:Router, private datosTr: TramiteModule, public dialog: MatDialog, private _tramiteService: TramiteEnviadoService, public actualizar: TramiteModule, private _snackbar:MatSnackBar ) {}
  cargarTramite(){
    this.lisTramite=this._tramiteService.getTramite();
    this.dataSource = new MatTableDataSource(this.lisTramite);
  }
  private intervalo:any
  ngOnInit(): void{
   this.cargarTramite();
   this.dataSource.data = this.dataSource.data.filter(element=>!this.datosTr.enviadoRecibido.includes(element));

   this.datosTr.tramiteN = this.datosTr.tramiteN.filter(element=>!this.datosTr.enviadoRecibido.includes(element));
   this.dataSource.data = this.actualizar.tramiteN.concat(this.dataSource.data);
   this.resp=[...this.dataSource.data]

   this.intervalo = setInterval(() => {
    this.filtrarGeneral();
  }, 1000);

}
filtrarGeneral(): void {
  this.dataSource.filter = this.svRecibidos.filtrado.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
  ////
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    console.log('ingreso',this.isAllSelected());
    if (this.isAllSelected()) {
      this.selection.clear(); ///deselecciona el checkk el listado si la respuesta es true
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: tramite): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
  }

  deleteElement(element: any) {
    const index = this.dataSource.data.indexOf(element);
    if (index >= 0) {
      const deletedElement = this.dataSource.data.splice(index, 1)[0]; // Elimina el elemento y obténlo
      this.datosTr.enviadoRecibido.unshift(deletedElement); // Agrega el elemento al arreglo de elementos eliminados
      this.dataSource.data = [...this.dataSource.data]; // Actualiza la referencia del arreglo de datos en la vista
    }
  }

  eliminarTramite(index:any,dataTram:any){
    console.log(index,dataTram);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { nrotramite: dataTram.nro_tramite,nrocopia:dataTram.nro_copia,asunto:dataTram.asunto };

    const dialogRef = this.dialog.open(ConfirmaDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {

        this.deleteElement(dataTram)
        // Realizar la eliminación del registro aquí
        // this._tramiteService.eliminarTramite(index);
        // this.cargarTramite();
        this._snackbar.open('Tramite Retractado Exitosamente','',{
              duration:1500,
              horizontalPosition:'center',
              verticalPosition:'bottom'
        });
        const currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
      }

    });
  }

openDialog(){
  console.log('openDialog');
  const dialogRef = this.dialog.open(HistorialComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  formatDate(date: Date): string {
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    } as Intl.DateTimeFormatOptions;
    const formattedDate = date.toLocaleString('en-US', options);
    const [datePart, timePart] = formattedDate.split(', ');
  
    const [month, day, year] = datePart.split('/');
    return `${year}-${month}-${day}`;
  }
  
  
  
   diaSeleccionado: string = 'Todas las fechas';

  resp : tramite[] = []
  
  selectDias(dias:string) {

    const primero: string []= []
    const segundo: string []= []
    const tercero: string []= []
  
    const fechaActual = new Date();
    
    
    for (let i =1 ; i<=21;i++){
      const fecha7DiasAntes = new Date();
      const fecha14DiasAntes = new Date();
      const fecha21DiasAntes = new Date();
      if (i<=7){
        
        fecha7DiasAntes.setDate(fechaActual.getDate() - i);
        primero.unshift(this.formatDate(fecha7DiasAntes))
      }else if(i<=14 && i>7){
        fecha14DiasAntes.setDate(fechaActual.getDate() - i);
        segundo.unshift(this.formatDate(fecha14DiasAntes))
        
      }else if(i>14 && i<=21){
        fecha21DiasAntes.setDate(fechaActual.getDate() - i);
        tercero.unshift(this.formatDate(fecha21DiasAntes))
        
      }
      
    }
  
    if (dias === "7") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${primero.join("|")}).*`);
      console.log('7');
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      this.diaSeleccionado="Últimos 7 días"
      
    } else if (dias === "14") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${segundo.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('14');
      this.diaSeleccionado="Últimos 14 días"
    } else if (dias === "21") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${tercero.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('21');
      this.diaSeleccionado="Últimos 21 días"
    } else if (dias === "todos"){
      this.dataSource.data = [...this.resp]
      this.diaSeleccionado="Todas las fechas"
    }
  }

}


@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html',
})
export class historialEnviadosComponent {}

