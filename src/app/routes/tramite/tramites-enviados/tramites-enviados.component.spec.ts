import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesEnviadosComponent } from './tramites-enviados.component';

describe('TramitesEnviadosComponent', () => {
  let component: TramitesEnviadosComponent;
  let fixture: ComponentFixture<TramitesEnviadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesEnviadosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramitesEnviadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
