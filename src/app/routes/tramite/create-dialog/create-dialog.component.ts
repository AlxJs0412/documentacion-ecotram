import { Component } from '@angular/core';
interface subTramites {
  sub: string;
}

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss']
})
export class CreateDialogComponent {
  mostrarComboBox1?: boolean;
  mostrarComboBox2?: boolean ;
  mostrarComboBox3?: boolean ;
  mostrarComboBox4?: boolean ;
  mostrarComboBox5?: boolean ;
  mostrarComboBox6?: boolean ;
  mostrarComboBox7?: boolean ;
  mostrarComboBox8?: boolean ;
  mostrarComboBox9?: boolean ;
  mostrarComboBox10?: boolean ;
  mostrarComboBox11?: boolean ;
  mostrarComboBox12?: boolean ;
  mostrarComboBox13?: boolean ;
  mostrarComboBox14?: boolean ;
  mostrarComboBox15?: boolean ;
  mostrarComboBox16?: boolean ;
  mostrarComboBox17?: boolean ;
  mostrarComboBox18?: boolean ;
  mostrarComboBox19?: boolean ;
  mostrarComboBox20?: boolean ;
  mostrarComboBox21?: boolean ;
  mostrarComboBox22?: boolean ;
  mostrarComboBox23?: boolean ;
  mostrarComboBox24?: boolean ;
  mostrarComboBox25?: boolean ;
  mostrarComboBox26?: boolean ;
  mostrarComboBox27?: boolean ;
  mostrarComboBox28?: boolean ;
  mostrarComboBox29?: boolean ;
  mostrarComboBox30?: boolean ;
  mostrarComboBox31?: boolean ;
  mostrarComboBox32?: boolean ;
  mostrarComboBox33?: boolean ;
  mostrarComboBox34?: boolean ;
  mostrarComboBox35?: boolean ;
  mostrarComboBox36?: boolean ;
  mostrarComboBox37?: boolean ;
  mostrarComboBox38?: boolean ;
  mostrarComboBox39?: boolean ;
  mostrarComboBox40?: boolean ;
  mostrarComboBox41?: boolean ;
  mostrarComboBox42?: boolean ;
  mostrarComboBox43?: boolean ;
  mostrarComboBox44?: boolean ;



  ngOnInit(){
    

  }


  act: subTramites[] = [
    {sub: 'ACTUALIZACIÓN'},
  ];
  //ADMINISTRACIÓN TERRITORIAL
  admt: subTramites[] = [
    {sub: 'REGULARIZACIÓN DE CONSTRUCCIONES FUERA DE NORMA'},
    {sub: 'VERIFICACIÓN DE TRAZO'},
    {sub: 'AS BUILT'},
    {sub: 'REGULACIÓN DE TRAZOS'},
  ];

  //CALIFICACIÓN AÑOS DE SERVICIO
  cas: subTramites[] = [
    {sub: 'CERTIFICACION APORTES FONVIS'},
    {sub: 'LEGALIZACION CERTIFICACION AÑOS DE SERVICIO'},
    {sub: 'CERTIFICACION BOLETAS DE PAGO'},
    {sub: 'CERTIFICACION HORAS EXTRA'},
    {sub: 'CERTIFICACION AÑOS DE SERVICIO'},
    {sub: 'COMPLEMENTACION AÑOS DE SERVICIO'},
  ];

  //CATASTRO
  cat: subTramites[]=[
    {sub: 'ACTUALIZACIÓN'},
    {sub: 'JUDICIALES'},
    {sub: 'LEGALIZACIONES'},
    {sub: 'NUEVO REGISTRO CATASTRAL'},
  ];

  //CATASTRO EN LINEA
  catel: subTramites[]=[
    {sub: 'PROFESIONAL EXTERNO - EN LINEA'},
    {sub: 'DUPLICADO CERTIFICADO CATASTRAL EN LINEA'},
  ];

  //CORRESPONDENCIA CIUDADANA CM
  cccm: subTramites[]=[
    {sub: 'CORRESPONDENCIA CIUDADANA CM'},
  ];

  //CORRESPONDENCIA CIUDADANA CM DIGITAL
  cccmd: subTramites[]=[
    {sub: 'CORRESPONDENCIA CIUDADANA CM DIGITAL'},
  ];

  //CARPETAS DE INVERSIÓN - GAMLP
  cdi: subTramites[]=[
    {sub: 'CARPETAS DE INVERSIÓN - GAMLP'},
  ];

  //CORRESPONDENCIA CIUDADANA
  corc: subTramites[]=[
    {sub: 'CORRESPONDENCIA CIUDADANA'},
  ];

  //CORRESPONDENCIA EJECUTIVO MUNICIPAL
  corr: subTramites[]=[
    {sub: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL'},
  ];

  //CORRESPONDENCIA CONCEJO MUNICIPAL
  crcm: subTramites[]=[
    {sub: 'CORRESPONDENCIA CONCEJO MUNICIPAL'},
  ];

  //COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS
  cvep: subTramites[] = [
    {sub: 'AUTORIZACION USO DE ESPACIOS PUBLICOS'},
    {sub: 'CERTIFICACION'},
    {sub: 'RECTIFICACIONES Y/O MODIFICACIONES'},
    {sub: 'REVERSION DE PUESTOS'},
    {sub: 'RETIRO DE PUESTOS'},
    {sub: 'PASO A TIENDAS Y DOMICILIOS PRIVADOS'},
    {sub: 'DESARCHIVO DE DOCUMENTACION'},
    {sub: 'CONCILIACION Y MEDIACION DE CONTROVERSIAS'},
    {sub: 'FOTOCOPIAS LEGALIZADAS'},
    {sub: 'REGULARIZACION DE REGISTRO'},
    {sub: 'AUTORIZACION CAMBIO DE NOMBRE'},
    {sub: 'DENUNCIA POR ASENTAMIENTOS'},
    {sub: 'OTROS'},
  ];

  //DECRETO EDIL
  de: subTramites[] = [
    {sub: 'DECRETO EDIL'}
  ];

  //DECRETO MUNICIPAL
  dm: subTramites[] =[
    { sub: 'DECRETO MUNICIPAL' }
  ]; 

  //DENUNCIAS Y RECLAMOS
  drtr: subTramites[] = [
    {sub: 'DENUNCIA'},
    {sub: 'MOVILIDAD Y TRANSPORTE'},
    {sub: 'RECLAMO'},
  ];

  //EJECUCIÓN PROCESOS DE FISCALIZACIÓN
  epfz: subTramites[] = [
    {sub: 'EJECUCIÓN PROCESOS DE FISCALIZACIÓN'}
  ];

  //FISCALIZACION TERRITORIAL
  fist: subTramites[] =[
    {sub: 'DENUNCIA VERBAL'},
    {sub: 'DENUNCIA ESCRITA'},
  ];

  //FOTOCOPIAS
  fot: subTramites[] =[
    {sub: 'LEGALIZADA'},
    {sub: 'SIMPLE'}
  ];

  //FISCALIZACIÓN TRIBUTOS MUNICIPALES
  ftm: subTramites[] = [
    {sub: 'VEHICULOS'},
    {sub: 'INMUEBLES'}, 
    {sub: 'ACTIVIDADES ECONOMICAS'}, 
    {sub: 'PUBLICIDAD URBANA'},  
  ];

  //INMUEBLES
  inm: subTramites[] = [
    {sub: 'EXENCION DE IMPUESTOS'},
    {sub: 'SUSPENSION DE TRAMITE'},
    {sub: 'DESBLOQUEO'},
    {sub: 'PRESCRIPCION DE IMPUESTOS'},
    {sub: 'OTRO'},
    {sub: 'CREDITO MUNICIPAL'},
    {sub: 'CAMBIO DE AÑO DE INICIO TRIBUTARIO'},
    {sub: 'BLOQUEO'},
    {sub: 'CERTIFICACIONES'},
    {sub: 'ANALISIS TECNICO TRIBUTARIO PARA INMUEBLES'},
    {sub: 'BAJAS EN GENERAL'}
  ];

  //LEY MUNICIPAL AUTONÓMICA
  lam: subTramites[] = [
    {sub: 'LEY MUNICIPAL AUTONÓMICA'},
  ];


  //MINUTAS DE COMUNICACIÓN
  mc: subTramites[] = [
    {sub: 'MINUTAS DE COMUNICACIÓN'},
  ];


  //MERCADOS MUNICIPALES DE ABASTO
  mma: subTramites[] = [
    {sub: 'DENUNCIAS'},
    {sub: 'DESARCHIVO DE DOCUMENTOS'},
    {sub: 'NUEVO REGISTRO Y ADJUDICACION EN MERCADOS ABASTO'},
    {sub: 'CERTIFICACIONES'},
    {sub: 'ACTUALIZACION DE DATOS DE REGISTRO'},
    {sub: 'LIMPIEZA Y FUMIGADO DE MERCADOS'},
    {sub: 'AUTORIZACION DE MANTENIMIENTO Y REFACCION MERCADOS'},
    {sub: 'FOTOCOPIAS LEGALIZADAS'},
    {sub: 'OTROS'},
    {sub: 'BAJA Y REVERSION DE REGISTRO'},
  ];

  //ORDEN DE DESPACHO
  od: subTramites[] = [
    {sub: 'ORDEN DE DESPACHO'},
  ];

  //ORDENANZA MUNICIPAL
  om: subTramites[] = [
    {sub: 'ORDENANZA MUNICIPAL'},
  ];

  //PUBLICIDAD
  p: subTramites[] = [
    {sub: 'CORPORATIVA POR SU USO'},
    {sub: 'MÓVIL'},
    {sub: 'EVENTUAL'},
    {sub: 'CORPORATIVA POR SU TAMAÑO'},
  ];

  //PETICIÓN DE INFORME ESCRITO
  pie: subTramites[] = [
    {sub: 'PETICIÓN DE INFORME ESCRITO'},
  ];

  //PETICIÓN DE INFORME ORAL
  pio: subTramites[] = [
    {sub: 'PETICIÓN DE INFORME ORAL'},
  ];

  //PLANILLA PAGO DE OBRAS
  ppo: subTramites[] = [
    {sub: 'PLANILLA PAGO DE OBRAS'},
  ];

  //PUBLICIDAD URBANA
  pub: subTramites[] = [
    {sub: 'C-IV. EVENTUAL PASACALLES'},
    {sub: 'C-III. MOVIL PINTADA'},
    {sub: 'COLOCADO DE LETRERO'},
    {sub: 'OTROS'},
    {sub: 'EXENCION DE PAGO PATENTE'},
    {sub: 'C-I. GIGANTOGRAFICA VALLA AUTOPORTANTE'},
    {sub: 'C-III. MOVIL LUMINOSA'},
    {sub: 'C-I. GIGANTOGRAFICA TOTEMS'},
    {sub: 'C-I. GIGANTOGRAFICA PANEL ELECTRONICO'},
    {sub: 'C-IV. EVENTUAL CARPAS'},
    {sub: 'PLAN DE PAGOS PUBLICIDAD URBANA'},
    {sub: 'C-IV EVENTUAL INFLABLES'},
    {sub: 'BAJA DE LA LICENCIA DE PUBLICIDAD'},
    {sub: 'RENOVACION LICENCIA DE PUBLICIDAD'},
    {sub: 'C-II. EXTERIOR FIJA ADOSADA'},
    {sub: 'C-II. EXTERIOR FIJA PINTADA'},
    {sub: 'C-II. EXTERIOR FIJA MICROPERFORADA'},
    {sub: 'C-II. EXTERIOR FIJA PALETA AUTOPORTANTE'},
    {sub: 'C-III. MOVIL AUTOADHESIVA'},
    {sub: 'C-IV. EVENTUAL AFICHES'},
    {sub: 'COLOCADO DE BANNERS'},
    {sub: 'LIQUIDACION DE PATENTES PUBLICIDAD URBANA'},
    {sub: 'DENUNCIA DE COLOCADO DE PUBLICIDAD'},
    {sub: 'C-I. GIGANTOGRAFICA VALLA ADOSADA'},
    {sub: 'C-I. GIGANTOGRAFICA MULTIPANTALLA'},
  ];

  //RESOLUCIÓN EJECUTIVA
  re: subTramites[] = [
    {sub: 'RESOLUCIÓN EJECUTIVA'},
  ];

  //REQUERIMIENTO DE ATENCIÓN
  rea: subTramites[] = [
    {sub: 'REQUERIMIENTO DE ATENCIÓN'},
  ];

  //REQUERIMIENTO DE INFORMACION
  rein: subTramites[] = [
    {sub: 'REQUERIMIENTO DE INFORMACION'},
  ];

  //RESOLUCIÓN MUNICIPAL CM
  rmcm: subTramites[] = [
    {sub: 'RESOLUCIÓN MUNICIPAL CM'},
  ];

  //RESOLUCIÓN MUNICIPAL
  rmu: subTramites[] = [
    {sub: 'RESOLUCIÓN MUNICIPAL'},
  ];

  //SESIONES INFORMATIVAS
  sinf: subTramites[] = [
    {sub: 'SESIONES INFORMATIVAS'},
  ];

  //SOLICITUD DE INFORMACIÓN – UT
  siut: subTramites[] = [
    {sub: 'SOLICITUD DE INFORMACIÓN – UT'},
  ];

  //SOLICITUD DE INFORMACIÓN
  soin: subTramites[] = [
    {sub: 'SOLICITUD DE INFORMACIÓN'},
  ];

  //TRANSFERENCIA DE ÁREA RESIDUAL
  tar: subTramites[] = [
    {sub: 'TRANSFERENCIA DE ÁREA RESIDUAL'},
  ];

  //TRAMITE DIGITAL
  td: subTramites[] = [
    {sub: 'TRAMITE DIGITAL'},
  ];
  
  //DENUNCIAS TRANSPARENCIA
  tra: subTramites[] = [
    {sub: 'DENUNCIAS TRANSPARENCIA'},
  ];
  
  //USO DE ESPACIOS PÚBLICOS TEMPORALES
  uepp: subTramites[] = [
    {sub: 'LUCRATIVAS (VENTA DE ARTÍCULOS O SERVICIOS)'},
    {sub: 'NO LUCRATIVAS (CULTURALES, RELIGIOSAS, ETC.)'},
  ];
  
  //VEHÍCULOS
  veh: subTramites[] = [
    {sub: 'ANÁLISIS TÉCNICO TRIBUTARIO PARA VEHÍCULOS'},
    {sub: 'ACTUALIZACIÓN DATOS CONTRIBUYENTE'},
    {sub: 'ANULACIÓN DE REGISTRO DE INSCRIPCIÓN'},
    {sub: 'ASIGNACIÓN DE PROPIEDAD/COPROPIEDAD'},
    {sub: 'TRANSFERENCIAS'},
    {sub: 'RECISIÓN, DESISTIMIENTO O DEVOLUCIÓN DE VENTA'},
    {sub: 'RELIQUIDACIONES IMTO'},
    {sub: 'PRESCRIPCIÓN DE IMPUESTOS'},
    {sub: 'CERTIFICACIONES'},
    {sub: 'OTRO'},
    {sub: 'EXENCIÓN DE IMPUESTOS'},
    {sub: 'CRÉDITO MUNICIPAL'},
    {sub: 'BAJA DE REGISTRO Y EMISIÓN CERTIFICADO DE BAJA'},
    {sub: 'ANULACIÓN DE RECIBO POR INSCRIPCIÓN'},
    {sub: 'BLOQUEO/DESBLOQUEO'},
    {sub: 'CAMBIO DE SERVICIO'},
  ];

  ocultarComboBox(opcion: number) {
    if (opcion === 1) {
      this.mostrarComboBox1 = !this.mostrarComboBox1;  
    } else if (opcion === 2) {
      this.mostrarComboBox2 = !this.mostrarComboBox2;
    }else if (opcion === 3) {
      this.mostrarComboBox3 = !this.mostrarComboBox3;
    }else if (opcion === 4) {
      this.mostrarComboBox4 = !this.mostrarComboBox4;
    }else if (opcion === 5) {
      this.mostrarComboBox5 = !this.mostrarComboBox5;
    }else if (opcion === 6) {
      this.mostrarComboBox6 = !this.mostrarComboBox6;
    }else if (opcion === 7) {
      this.mostrarComboBox7 = !this.mostrarComboBox7;
    }else if (opcion === 8) {
      this.mostrarComboBox8 = !this.mostrarComboBox8;
    }else if (opcion === 9) {
      this.mostrarComboBox9 = !this.mostrarComboBox9;
    }else if (opcion === 10) {
      this.mostrarComboBox10 = !this.mostrarComboBox10;
    }else if (opcion === 11) {
      this.mostrarComboBox11 = !this.mostrarComboBox11;
    }else if (opcion === 12) {
      this.mostrarComboBox12 = !this.mostrarComboBox12;
    }else if (opcion === 13) {
      this.mostrarComboBox13 = !this.mostrarComboBox13;
    }else if (opcion === 14) {
      this.mostrarComboBox14 = !this.mostrarComboBox14;
    }else if (opcion === 15) {
      this.mostrarComboBox15 = !this.mostrarComboBox15;
    }else if (opcion === 16) {
      this.mostrarComboBox16 = !this.mostrarComboBox16;
    }else if (opcion === 17) {
      this.mostrarComboBox17 = !this.mostrarComboBox17;
    }else if (opcion === 18) {
      this.mostrarComboBox18 = !this.mostrarComboBox18;
    }else if (opcion === 19) {
      this.mostrarComboBox19 = !this.mostrarComboBox19;
    }else if (opcion === 20) {
      this.mostrarComboBox20 = !this.mostrarComboBox20;
    }else if (opcion === 21) {
      this.mostrarComboBox21 = !this.mostrarComboBox21;
    }else if (opcion === 22) {
      this.mostrarComboBox22 = !this.mostrarComboBox22;
    }else if (opcion === 23) {
      this.mostrarComboBox23 = !this.mostrarComboBox23;
    }else if (opcion === 24) {
      this.mostrarComboBox24 = !this.mostrarComboBox24;
    }else if (opcion === 25) {
      this.mostrarComboBox25 = !this.mostrarComboBox25;
    }else if (opcion === 26) {
      this.mostrarComboBox26 = !this.mostrarComboBox26;
    }else if (opcion === 27) {
      this.mostrarComboBox27 = !this.mostrarComboBox27;
    }else if (opcion === 28) {
      this.mostrarComboBox28 = !this.mostrarComboBox28;
    }else if (opcion === 29) {
      this.mostrarComboBox29 = !this.mostrarComboBox29;
    }else if (opcion === 30) {
      this.mostrarComboBox30 = !this.mostrarComboBox30;
    }else if (opcion === 31) {
      this.mostrarComboBox31 = !this.mostrarComboBox31;
    }else if (opcion === 32) {
      this.mostrarComboBox32 = !this.mostrarComboBox32;
    }else if (opcion === 33) {
      this.mostrarComboBox33 = !this.mostrarComboBox33;
    }else if (opcion === 34) {
      this.mostrarComboBox34 = !this.mostrarComboBox34;
    }else if (opcion === 35) {
      this.mostrarComboBox35 = !this.mostrarComboBox35;
    }else if (opcion === 36) {
      this.mostrarComboBox36 = !this.mostrarComboBox36;
    }else if (opcion === 37) {
      this.mostrarComboBox37 = !this.mostrarComboBox37;
    }else if (opcion === 38) {
      this.mostrarComboBox38 = !this.mostrarComboBox38;
    }else if (opcion === 39) {
      this.mostrarComboBox39 = !this.mostrarComboBox39;
    }else if (opcion === 40) {
      this.mostrarComboBox40 = !this.mostrarComboBox40;
    }else if (opcion === 41) {
      this.mostrarComboBox41 = !this.mostrarComboBox41;
    }else if (opcion === 42) {
      this.mostrarComboBox42 = !this.mostrarComboBox42;
    }else if (opcion === 43) {
      this.mostrarComboBox43 = !this.mostrarComboBox43;
    }else if (opcion === 44) {
      this.mostrarComboBox44 = !this.mostrarComboBox44;
    }
    
  }

}
