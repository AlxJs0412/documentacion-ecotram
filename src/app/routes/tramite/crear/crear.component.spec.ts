import { Component } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { BuscarPersonaComponent } from '../buscar-persona/buscar-persona.component';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.scss']
})
export class CrearComponent {
  constructor(public buscarPersona: MatDialog){}
  openBuscarPersona(){

    const dialogRef = this.buscarPersona.open(BuscarPersonaComponent);

      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }
}
