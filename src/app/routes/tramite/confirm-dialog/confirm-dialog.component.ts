import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  nrotramite: string;
  nrocopia:string;
  asunto:string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { 
    this.nrotramite = data.nrotramite;
    this.nrocopia = data.nrocopia;
    this.asunto = data.asunto;
  }
}
