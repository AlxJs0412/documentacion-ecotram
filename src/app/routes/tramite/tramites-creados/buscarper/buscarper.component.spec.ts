import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarperComponent } from './buscarper.component';

describe('BuscarperComponent', () => {
  let component: BuscarperComponent;
  let fixture: ComponentFixture<BuscarperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuscarperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
