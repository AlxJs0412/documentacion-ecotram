import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesCreadosComponent } from './tramites-creados.component';

describe('TramitesCreadosComponent', () => {
  let component: TramitesCreadosComponent;
  let fixture: ComponentFixture<TramitesCreadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesCreadosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramitesCreadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
