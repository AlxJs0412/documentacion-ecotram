import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCopy2Component } from './crear-copy2.component';

describe('CrearCopy2Component', () => {
  let component: CrearCopy2Component;
  let fixture: ComponentFixture<CrearCopy2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearCopy2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearCopy2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
