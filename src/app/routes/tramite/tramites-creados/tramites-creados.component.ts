import { Component, Inject, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { TramitesService } from 'app/services/tramites.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateDialogComponent } from '../create-dialog/create-dialog.component';

import { MatSnackBar } from '@angular/material/snack-bar';
import { NuevoTramiteComponent } from './nuevo-tramite/nuevo-tramite.component';
import { Confirm2DialogComponent } from './confirm2-dialog/confirm2-dialog.component';
import { TramiteNuevoComponent } from './tramite-nuevo/tramite-nuevo.component';
import { Router } from '@angular/router';
import { MatTabGroup } from '@angular/material/tabs';
import { TramiteModule } from '../tramite.module';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';
import { EnviarTodosComponent } from '../tramites-recibidos/enviar-todos/enviar-todos.component';



export interface tramite {
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  borrador: string,
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}

@Component({
  selector: 'app-tramites-creados',
  templateUrl: './tramites-creados.component.html',
  styleUrls: ['./tramites-creados.component.scss']
})
export class TramitesCreadosComponent {
  lisTramite:tramite[]=[];
  datos: any;
  displayedColumns: string[] = ['select','nro_tramite','nro_copia','cite','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad','opciones'];
  // lisTramite: tramite[] = [
  //   {nro_tramite:23,nro_copia:'ORIGINAL',asunto: 'MAMANI PEREZ JUAN- SEGUIMIENTO ',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-05-18 16:44:14',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Urgente'},
  //   {nro_tramite:23,nro_copia:'ORIGINAL',asunto: 'MAMANI PEREZ JUAN- SEGUIMIENTO ',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-05-18 16:44:14',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Baja'},
  //   {nro_tramite:23,nro_copia:'ORIGINAL',asunto: 'MAMANI PEREZ JUAN- SEGUIMIENTO ',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-05-18 16:44:14',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Urgente'},
  //   {nro_tramite:23,nro_copia:'ORIGINAL',asunto: 'MAMANI PEREZ JUAN- SEGUIMIENTO ',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-05-18 16:44:14',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Media'} 
  // ];
  
  // datos: any;
  // //displayedColumns: string[] = ['acciones','nro_tramite','nro_copia','asunto','tipo_tramite','procedencia','fecha_creacion','fecha_modificacion','tomado_por','so_sistema_nombre','dias_atencion','prioridad'];
  // displayedColumns: string[] = ['select','nro_tramite','nro_copia','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad','opciones'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // dataSource: MatTableDataSource < any > = new MatTableDataSource < any > ([]);
  dataSource=new  MatTableDataSource(this.lisTramite);
  @ViewChild(MatSort) sort = new MatSort();
  selection = new SelectionModel<tramite>(true, []);
  ////combo
  selectedValue: string | undefined;
 

  constructor(private svRecibidos: TramiteRecibidoService,public dialog: MatDialog, private _snackbar:MatSnackBar, private _tramiteService: TramitesService,private router: Router, private matTabGroup: MatTabGroup,public prueba: TramiteModule){

  }

  contador: number = 0;

  actualizarContador(checked: boolean) {
    if (checked) {
      this.contador++;
    }else {
      this.contador--;
    }
  }

  resetCont(checked: boolean){
    if(checked){
      this.contador = 2;
    }else{
      this.contador = 0;
    }
  }

  
  getSelectedRows() {

    this.dataSource.data.forEach((element) => {
      if (this.selection.isSelected(element)) {
        this.svRecibidos.tramitesEnvioBloque.push(element);
      }
    });

    console.log(this.svRecibidos.tramitesEnvioBloque)
  }
  openBloque(){
    this.svRecibidos.tramitesEnvioBloque= []
    this.getSelectedRows()
    const dialogRef = this.dialog.open(EnviarTodosComponent);
  
    dialogRef.afterClosed().subscribe(result => {
      if(this.svRecibidos.controlBloque==true){
        this.prueba.tramiteN.unshift(...this.svRecibidos.tramitesEnvioBloque)
          this.dataSource.data = this.dataSource.data.filter((elemento) => !this.svRecibidos.tramitesEnvioBloque.includes(elemento));
          const currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
          this.cargarTramite();
        }
        this.svRecibidos.controlBloque=false;
        this.svRecibidos.enviado=true;
        this.cargarTramite();
        
      })
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
  }
  } 
  intervalo:any
  ngOnInit(){
    this.cargarTramite();
    this.dataSource.data = this.dataSource.data.filter(element=>!this.prueba.tramiteN.includes(element));
    this.dataSource.data = this.svRecibidos.datosBorrador.concat(this.dataSource.data);
    this.resp=[...this.dataSource.data]
 this.intervalo = setInterval(() => {
  this.filtrarGeneral();
}, 1000);

}
filtrarGeneral(): void {
this.dataSource.filter = this.svRecibidos.filtrado.trim().toLowerCase();
if (this.dataSource.paginator) {
  this.dataSource.paginator.firstPage();
}
}

  ////
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    console.log('ingreso',this.isAllSelected());
    if (this.isAllSelected()) {
      this.selection.clear(); ///deselecciona el checkk el listado si la respuesta es true
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

/** The label for the checkbox on the passed row */
  checkboxLabel(row?: tramite): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
  }

  
  openDialog(): void {
    this.dialog.open(CreateDialogComponent);
  }

 

  cargarTramite(){
    this.lisTramite = this._tramiteService.getTramite();
    this.dataSource = new MatTableDataSource(this.lisTramite);
  }

  eliminarTramite(index:any,dataTram:any){
    console.log(index,dataTram);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { nrotramite: dataTram.nro_tramite,nrocopia:dataTram.nro_copia,asunto:dataTram.asunto };
    const dialogRef = this.dialog.open(Confirm2DialogComponent, dialogConfig);
  
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        // Realizar la eliminación del registro aquí
        this._tramiteService.eliminarTramite(index);
        this.cargarTramite();
        this._snackbar.open('Tramite Revertido Exitosamente','',{
              duration:1500,
              horizontalPosition:'center',
              verticalPosition:'bottom'
        });
      }
  
    });
  }


  deleteElement(element: any) {
    const index = this.dataSource.data.indexOf(element);
    if (index >= 0) {
      // this.dataSource.data.splice(index, 1);
      // this.dataSource.data = [...this.dataSource.data];
  
      const deletedElement = this.dataSource.data.splice(index, 1)[0]; // Elimina el elemento y obténlo
      this.prueba.tramiteN.unshift(deletedElement); // Agrega el elemento al arreglo de elementos eliminados
      this.dataSource.data = [...this.dataSource.data]; // Actualiza la referencia del arreglo de datos en la vista
    }
  }

  redirigirTab(event: any){
    console.log('Se ha activado el evento redirigirTabEvent');
  console.log('Información del evento:', event);

  }

  nuevoTramite(elm : any){
    console.log('openDialog');
  const dialogRef = this.dialog.open(NuevoTramiteComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if(this.prueba.control==true){
        this.deleteElement(elm);
        const currentUrl = this.router.url;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);
        this.cargarTramite();
        this._snackbar.open('Tramite Enviado Exitosamente','',{
          duration:1500,
          horizontalPosition:'center',
          verticalPosition:'bottom'});
      }
    });

  }

  
  crearTramite(){
    const dialogRef = this.dialog.open(TramiteNuevoComponent);
    
    dialogRef.afterClosed().subscribe(result => {
      if(this.svRecibidos.borrador==true){
        this.svRecibidos.datosBorrador.unshift({nro_tramite:6622,nro_copia:'ORIGINAL', asunto: this.svRecibidos.asuntoBorrador ,borrador:'- BORRADOR',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-04-12 11:20:43',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Alta'})
        const currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
          this.cargarTramite();
      }
      this.svRecibidos.creado=true;
      this.svRecibidos.borrador=false;
      this.cargarTramite();
      this.ngOnInit()
    })

  }


  formatDate(date: Date): string {
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    } as Intl.DateTimeFormatOptions;
    const formattedDate = date.toLocaleString('en-US', options);
    const [datePart, timePart] = formattedDate.split(', ');
  
    const [month, day, year] = datePart.split('/');
    return `${year}-${month}-${day}`;
  }
  
  
  
   diaSeleccionado: string = 'Todas las fechas';

  resp : tramite[] = []
  
  selectDias(dias:string) {

    const primero: string []= []
    const segundo: string []= []
    const tercero: string []= []
  
    const fechaActual = new Date();
    
    
    for (let i =1 ; i<=21;i++){
      const fecha7DiasAntes = new Date();
      const fecha14DiasAntes = new Date();
      const fecha21DiasAntes = new Date();
      if (i<=7){
        
        fecha7DiasAntes.setDate(fechaActual.getDate() - i);
        primero.unshift(this.formatDate(fecha7DiasAntes))
      }else if(i<=14 && i>7){
        fecha14DiasAntes.setDate(fechaActual.getDate() - i);
        segundo.unshift(this.formatDate(fecha14DiasAntes))
        
      }else if(i>14 && i<=21){
        fecha21DiasAntes.setDate(fechaActual.getDate() - i);
        tercero.unshift(this.formatDate(fecha21DiasAntes))
        
      }
      
    }
  
    if (dias === "7") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${primero.join("|")}).*`);
      console.log('7');
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      this.diaSeleccionado="Últimos 7 días"
      
    } else if (dias === "14") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${segundo.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('14');
      this.diaSeleccionado="Últimos 14 días"
    } else if (dias === "21") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${tercero.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('21');
      this.diaSeleccionado="Últimos 21 días"
    } else if (dias === "todos"){
      this.dataSource.data = [...this.resp]
      this.diaSeleccionado="Todas las fechas"
    }
  }

}



