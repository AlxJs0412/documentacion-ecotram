import {SelectionModel} from '@angular/cdk/collections';
import { Component } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-fusion',
  templateUrl: './fusion.component.html',
  styleUrls: ['./fusion.component.scss']
})
export class FusionComponent {
  

  displayedColumnsPadre: string[] = ['select','tramite', 'copias', 'asunto', 'tipo', 'procedencia', 'fecha_envio', 'ingresado', 'prioridad'];
  dataSourcePadre = new MatTableDataSource<NodoPadre>(NODO_PADRE);
  selectionPadre = new SelectionModel<NodoPadre>(true, []);

  displayedColumnsHijo: string[] = ['select','tramite', 'copias', 'asunto', 'tipo', 'procedencia', 'fecha_envio', 'ingresado', 'prioridad'];
  dataSourceHijo = new MatTableDataSource<NodoHijo>(NODO_HIJO);
  selectionHijo = new SelectionModel<NodoHijo>(true, []);

  getSelectedRows(selection: SelectionModel<NodoHijo>, data: NodoHijo[]): NodoHijo[] {
    
    const selectedRows: NodoHijo[] = [];
    selectedRows.splice(0, selectedRows.length);
  
    data.forEach((element) => {
      if (selection.isSelected(element)) {
        selectedRows.push(element);
      }
    });
    const selectedRowsPadre: NodoPadre[] = selectedRows.map((element) => ({
      tramite: element.tramite,
      copias: element.copias,
      asunto: element.asunto,
      tipo: element.tipo,
      procedencia: element.procedencia,
      fecha_envio: element.fecha_envio,
      tomado_por: element.tomado_por,
      prioridad: element.prioridad,
    }));
    this.dataSourcePadre.data = [...this.dataSourcePadre.data, ...selectedRowsPadre];

     // Eliminar las filas seleccionadas de la tabla hijo
    selectedRows.forEach((element) => {
      const index = this.dataSourceHijo.data.indexOf(element);
      if (index > -1) {
        this.dataSourceHijo.data.splice(index, 1);
      }
    });

    // Actualizar el dataSourceHijo para refrescar la tabla
    this.dataSourceHijo.data = this.dataSourceHijo.data.slice();  

    // Vaciar el array selectedRows para la próxima ejecución
    // selectedRows.splice(0, selectedRows.length);

    return selectedRows;
    
  }
  
  moverElementos() {
    const elementosSeleccionados = this.selectionPadre.selected;
  
    if (elementosSeleccionados.length > 0) {
      const elementosNoSeleccionados = this.dataSourcePadre.data.filter((element, index) => index !== 0 && !this.selectionPadre.isSelected(element));
  
      this.dataSourceHijo.data = [...this.dataSourceHijo.data, ...elementosSeleccionados];
      this.dataSourcePadre.data = [this.dataSourcePadre.data[0], ...elementosNoSeleccionados];
      this.selectionPadre.clear(); // Limpiar la selección después de mover los elementos
    }
}


verLog(){
  const selectedRows2 = this.getSelectedRows(this.selectionHijo, NODO_HIJO);
  console.log(selectedRows2);
  
}

/** Whether the number of selected elements matches the total number of rows. */
isAllSelectedPadre() {
  const numSelected = this.selectionPadre.selected.length;
  const numRows = this.dataSourcePadre.data.length;
  return numSelected === numRows;
}

/** Selects all rows if they are not all selected; otherwise clear selection. */
toggleAllRowsPadre() {
  if (this.isAllSelectedPadre()) {
    this.selectionPadre.clear();
    return;
  }

  this.selectionPadre.select(...this.dataSourcePadre.data);
}

/** The label for the checkbox on the passed row */
checkboxLabelPadre(row?: NodoPadre): string {
  if (!row) {
    return `${this.isAllSelectedPadre() ? 'deselect' : 'select'} all`;
  }
  return `${this.selectionPadre.isSelected(row) ? 'deselect' : 'select'} row ${row.tramite + 1}`;
}

isAllSelectedHijo() {
  const numSelected = this.selectionHijo.selected.length;
  const numRows = this.dataSourceHijo.data.length;
  return numSelected === numRows;
}

/** Selects all rows if they are not all selected; otherwise clear selection. */
toggleAllRowsHijo() {
  if (this.isAllSelectedHijo()) {
    this.selectionHijo.clear();
    return;
  }

  this.selectionHijo.select(...this.dataSourceHijo.data);
}

/** The label for the checkbox on the passed row */
checkboxLabelHijo(row?: NodoHijo): string {
  if (!row) {
    return `${this.isAllSelectedHijo() ? 'deselect' : 'select'} all`;
  }
  return `${this.selectionPadre.isSelected(row) ? 'deselect' : 'select'} row ${row.tramite + 1}`;
}
}

export interface NodoPadre{
  tramite: number;
  copias: string;
  asunto: string;
  tipo: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  prioridad: string;
}

const NODO_PADRE: NodoPadre[] = [
  { tramite: 26032, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 053/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', 
  tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL', procedencia: 'Interno', fecha_envio: '2022 07 26 17:31:42', tomado_por: 'm.tellez', prioridad:'Alta'},
];


export interface NodoHijo{
  tramite: number;
  copias: string;
  asunto: string;
  tipo: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  prioridad: string;
}

const NODO_HIJO: NodoHijo[] = [
  { tramite: 10001, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 023/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' , procedencia: 'Interno', fecha_envio: '2022 07 26 17:31:42', tomado_por: 'm.tellez', prioridad:'Alta' },
  { tramite: 10002, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' , procedencia: 'Interno', fecha_envio: '2022 07 26 17:31:42', tomado_por: 'm.tellez', prioridad:'Baja'},
  { tramite: 10003, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' , procedencia: 'Interno', fecha_envio: '2022 07 26 17:31:42', tomado_por: 'm.tellez', prioridad:'Alta'},
  { tramite: 10004, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' , procedencia: 'Interno', fecha_envio: '2022 07 26 17:31:42', tomado_por: 'm.tellez', prioridad:'Media'},
];
