import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Fusion2Component } from './fusion2.component';

describe('Fusion2Component', () => {
  let component: Fusion2Component;
  let fixture: ComponentFixture<Fusion2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Fusion2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Fusion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
