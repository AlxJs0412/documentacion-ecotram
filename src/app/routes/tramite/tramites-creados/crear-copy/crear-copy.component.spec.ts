import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCopyComponent } from './crear-copy.component';

describe('CrearCopyComponent', () => {
  let component: CrearCopyComponent;
  let fixture: ComponentFixture<CrearCopyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearCopyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
