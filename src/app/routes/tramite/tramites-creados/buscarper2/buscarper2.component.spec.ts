import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Buscarper2Component } from './buscarper2.component';

describe('Buscarper2Component', () => {
  let component: Buscarper2Component;
  let fixture: ComponentFixture<Buscarper2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Buscarper2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Buscarper2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
