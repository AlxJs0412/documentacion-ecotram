import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Confirm2DialogComponent } from './confirm2-dialog.component';

describe('Confirm2DialogComponent', () => {
  let component: Confirm2DialogComponent;
  let fixture: ComponentFixture<Confirm2DialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Confirm2DialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Confirm2DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
