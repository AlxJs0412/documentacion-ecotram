import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

import { FormControl} from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { TramiteModule } from '../../tramite.module';
import { Fusion2Component } from '../fusion2/fusion2.component';
import { Buscarper2Component } from '../buscarper2/buscarper2.component';
import { CrearCopy2Component } from '../crear-copy2/crear-copy2.component';

@Component({
  selector: 'app-nuevo-tramite',
  templateUrl: './nuevo-tramite.component.html',
  styleUrls: ['./nuevo-tramite.component.scss']
})
export class NuevoTramiteComponent {
  
  fruitCtrl = new FormControl('');
  filteredFruits: Observable<string[]>;
  // fruits: string[] = ['Oscar Martinez Ramos / DTIGA / Asistente Legal'];
  allFruits: string[] = ['susy.foronda / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 'timoteo.apaza / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 'cborda / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS','pedro.fuentes / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 
   'teofilo.tito / Asistente Legal / DESPACHO ALCALDE MUNICIPAL', 'juan.zurita / Tecnico Administrativo / DESPACHO ALCALDE MUNICIPAL', 'rodrigo.fernandez / Asistente Legal / DESPACHO ALCALDE MUNICIPAL','bmendoza / Asistente Legal / DESPACHO ALCALDE MUNICIPAL', 'isabel.bellot / Secretaria / DESPACHO ALCALDE MUNICIPAL',
   'adhemar.lima / Asistente Legal / RESPONSABLE ADMINISTRATIVO', 'karin.villavicencio / Asistente Legal / RESPONSABLE ADMINISTRATIVO', 
   'meflores / Asistente Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ','jorge.gomez / Asistente Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ','maria.palacios / Asisten Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ',
   'ramiro.castro / Asisten Legal / UNIDAD DE PROCESOS JURISDICCIONALES DAJ',
   'misael.mayta / Asistente Legal / ARCHIVO UPJ DAJ', 'leslie.santiago / Asistente Legal / ARCHIVO UPJ DAJ', 'mario.salas / Asistente Legal / ARCHIVO UPJ DAJ', 'mario.salas / Asistente Legal / ASISTENTE LEGAL UPJ DAJ',
   'leslie.santiago / Asistente Legal / ASISTENTE TECNICO UPJ DAJ'];

 //form: FormGroup;

  @ViewChild('fruitInput')
  fruitInput!: ElementRef<HTMLInputElement>;
  buttonLabel?: string = 'Acción 1'; 
  isButtonClicked?: boolean = false; 

  constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<NuevoTramiteComponent>,@Inject(MAT_DIALOG_DATA) public data: any, public control : TramiteModule){
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allFruits.slice())),
    );

    // this.form = this.fb.group({

    //   nro_tramite: data.nrotramite,
    //   nro_copia: data.nrocopia,
    //   asunto: data.asunto,
    //   borrador: '',
    //   tipo_tramite: data.tipotramite,
    //   procedencia:data.procedencia,
    //   fecha_envio:data.fechaenvio,
    //   tomado_por:data.tomadopor,
    //   so_sistema_nombre:data.sosistemanombre,
    //   dias_atencion:data.diasatencion,
    //   prioridad:data.prioridad,
    //   });
     
  }
 
  buscarPersona(){
    //cuando se presiona el boton buscar persona
     const dialogRef = this.dialog.open(Buscarper2Component);

       dialogRef.afterClosed().subscribe(result => {
         console.log(`Dialog result: ${result}`);
       });
      }
      
  de = [
    { 
      name: 'Oscar Martinez Ramos',
      dep: 'SEM',
      car: 'Asistente Legal'
    },
  ];

  para = [
    { 
      name: 'leslie.santiago',
      dep: 'UNIDAD DE PROCESOS JURIDICCIONALES ',
      car: 'ASISTENTE TECNICO UPJ DAJ'
    }
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.de.push({name: value,dep: value,car: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  add2(event2: MatChipInputEvent): void {
    const value = (event2.value || '').trim();

    // Add our fruit
    if (value) {
      this.para.push({name: value,dep: value,car: value});
    }

    // Clear the input value
    event2.chipInput!.clear();
  }

  remove(des: any): void {
    const index = this.de.indexOf(des);
    

    if (index >= 0) {
      this.de.splice(index, 1);
     
    }
  }

  remove2(paras: any): void {
   
    const index = this.para.indexOf(paras);

    if (index >= 0) {
  
      this.para.splice(index, 1);
    }
  }

 
  isInputVisible?: boolean = false;
  fruits = [...this.control.copiasNombres];
  copias() {
    const dialogRef =  this.dialog.open(CrearCopy2Component);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);   
      this.fruits=[...this.control.copiasNombres];
        console.log(this.fruits); 
        
    });
    
  }

  limpiarCopias(nombre: any){
    
    const indice = this.fruits.indexOf(nombre);
    if (indice !== -1) {
      this.fruits.splice(indice, 1);
    }

    console.log(nombre);
  }

  //del fruitscxczxczxcx

  add3(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.fruitCtrl.setValue(null);
  }

  remove3(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }

   agregar(){
    
    //  const tramites: tramite = {
    //    nro_tramite: this.form.value.nro_tramite,
    //    nro_copia: this.form.value.nro_copia,
    //    asunto: this.form.value.asunto,
    //    borrador: this.form.value.borrador,
    //    tipo_tramite: this.form.value.tipo_tramite,
    //    procedencia: this.form.value.procedencia,
    //    fecha_envio: this.form.value.fecha_envio,
    //    tomado_por: this.form.value.tomado_por,
    //    so_sistema_nombre: this.form.value.so_sistema_nombre,
    //    dias_atencion: this.form.value.dias_atencion,
    //    prioridad: this.form.value.prioridad,
    //  };
    //  this.router.navigate(['/tramite/bandeja-tabs']);
     this.dialogRef.close();
     this.control.control=true;
    //  this._tramiteService.agregarTramite(tramites);
    //   const currentUrl = this.router.url;
    //   this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    //   this.router.onSameUrlNavigation = 'reload';
    //   this.router.navigate([currentUrl]);
    //     this._snackBar.open('Tramite Enviado Exitosamente','',{
    //       duration:1500,
    //       horizontalPosition:'center',
    //       verticalPosition:'bottom'
    //       });
  }

  openFusionar(): void {
    const dialogRef =  this.dialog.open(Fusion2Component);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (this.isButtonClicked) {
        // Acción para el segundo clic
        console.log('Segundo clic');
  
        this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
      } else {
        // Acción para el primer clic
        console.log('Primer clic');
        // Realiza la lógica deseada para el primer clic
   
        this.buttonLabel = 'Acción 2'; // Cambia la etiqueta del botón para indicar la próxima acción en el segundo clic
        this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
      }
    
    });
  }

  buttonClicked(): void {
    if (this.isButtonClicked) {
      // Acción para el segundo clic
      console.log('Segundo clic');
      
      // const currentUrl = this.router.url;
      // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      // this.router.onSameUrlNavigation = 'reload';
      // this.router.navigate([currentUrl]);
      //   this._snackBar.open('Tramite Enviado Exitosamente','',{
      //     duration:1500,
      //     horizontalPosition:'center',
      //     verticalPosition:'bottom'
      //     });
      
      this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
    } else {
      // Acción para el primer clic
      console.log('Primer clic');
      // Realiza la lógica deseada para el primer clic
      this.buttonLabel = 'Acción 2'; // Cambia la etiqueta del botón para indicar la próxima acción en el segundo clic
      this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
      this.isInputVisible = !this.isInputVisible;
    }
  }



  


}
