import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramiteNuevoComponent } from './tramite-nuevo.component';

describe('TramiteNuevoComponent', () => {
  let component: TramiteNuevoComponent;
  let fixture: ComponentFixture<TramiteNuevoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramiteNuevoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramiteNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
