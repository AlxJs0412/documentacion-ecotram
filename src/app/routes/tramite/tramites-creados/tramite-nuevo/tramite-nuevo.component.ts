import { Component, ElementRef, Inject, ViewChild, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { tramite } from '../tramites-creados.component';
import { TramitesService } from 'app/services/tramites.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TramiteEnviadoService } from 'app/services/tramite-enviado.service';
import { FusionComponent } from '../fusion/fusion.component';
import { BuscarperComponent } from '../buscarper/buscarper.component';
import { CrearCopyComponent } from '../crear-copy/crear-copy.component';
import { TramiteModule } from '../../tramite.module';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';

export interface fruit{
  name: string;
  // direccion: string;
  // cargo: string;
}

@Component({
  selector: 'app-tramite-nuevo',
  templateUrl: './tramite-nuevo.component.html',
  styleUrls: ['./tramite-nuevo.component.scss']
})


export class TramiteNuevoComponent {

  asunto: string=""
  fruitCtrl = new FormControl<string | fruit>('');
  allFruits: fruit[] = [{name: 'susy.foronda'},{name: 'timoteo.apaza'}];
  filteredFruits?: Observable<fruit[]>;
  // ['susy.foronda / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 'timoteo.apaza / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 'cborda / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS','pedro.fuentes / Asistente Legal / DIRECCION DE ASUNTOS JURIDICOS', 
  //   'teofilo.tito / Asistente Legal / DESPACHO ALCALDE MUNICIPAL', 'juan.zurita / Tecnico Administrativo / DESPACHO ALCALDE MUNICIPAL', 'rodrigo.fernandez / Asistente Legal / DESPACHO ALCALDE MUNICIPAL','bmendoza / Asistente Legal / DESPACHO ALCALDE MUNICIPAL', 'isabel.bellot / Secretaria / DESPACHO ALCALDE MUNICIPAL',
  //   'adhemar.lima / Asistente Legal / RESPONSABLE ADMINISTRATIVO', 'karin.villavicencio / Asistente Legal / RESPONSABLE ADMINISTRATIVO', 
  //   'meflores / Asistente Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ','jorge.gomez / Asistente Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ','maria.palacios / Asisten Legal / UNIDAD DE ANÁLISIS LEGAL Y NORMATIVO DAJ',
  //   'ramiro.castro / Asisten Legal / UNIDAD DE PROCESOS JURISDICCIONALES DAJ',
  //   'misael.mayta / Asistente Legal / ARCHIVO UPJ DAJ', 'leslie.santiago / Asistente Legal / ARCHIVO UPJ DAJ', 'mario.salas / Asistente Legal / ARCHIVO UPJ DAJ', 'mario.salas / Asistente Legal / ASISTENTE LEGAL UPJ DAJ',
  //   'leslie.santiago / Asistente Legal / ASISTENTE TECNICO UPJ DAJ'];
   form: FormGroup;
   
   
  @ViewChild('fruitInput')
  fruitInput!: ElementRef<HTMLInputElement>;
  matTabGroup: any;
  buttonLabel?: string = 'Acción 1'; 
  isButtonClicked?: boolean = false; 
  
  constructor(private svRecibidos: TramiteRecibidoService,private fb: FormBuilder, public dialog: MatDialog,public dialogRef: MatDialogRef<TramiteNuevoComponent>, private router: Router, 
    private _tramiteService: TramiteEnviadoService,private _tramiteService2: TramitesService, private _snackbar:MatSnackBar, public control : TramiteModule,
    private lk: TramiteModule){
    // this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
    //   startWith(null),
    //   map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allFruits.slice())),
    // );
    
    
    this.form = this.fb.group({
    nro_tramite: 148,
    nro_copia:'ORIGINAL',
    asunto:'PRUEBA - REMISION DE FACTURAS',
    borrador: '- BORRADOR',
    tipo_tramite:'CORRESPONDENCIA CIUDADANA',
    procedencia:'EXTerno',
    fecha_envio:'2023-04-12 11:20:43',
    tomado_por:'leslie.margot',
    so_sistema_nombre:'ECOTRAM',
    dias_atencion:'3',
    prioridad:'Urgente',
    });

  }

  
  isInputVisible?: boolean = false;
  
  fruitslk = [...this.lk.aaa];
  fruits = [...this.control.copiasNombres];
  datosCopia=[...this.control.copiasNombres];

  ngOnInit(){
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.allFruits.slice();
      }),
    );
  }

  cargarDato(){
    this.svRecibidos.borrador=true;
    this.svRecibidos.asuntoBorrador=this.asunto;
    console.log(this.svRecibidos.asuntoBorrador)
  }
 
  buscarPersona(){
    //cuando se presiona el boton buscar persona
    const dialogRef =  this.dialog.open(BuscarperComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);  
      this.fruitslk=[...this.lk.aaa];
        console.log(this.fruitslk); 
       
    });
  
    }
      
  de = [{ name: 'Oscar Martinez Ramos',dep: 'SEM',car: 'Asistente Legal'}];

  para = [{ name: 'leslie.santiago',dep: 'UNIDAD DE PROCESOS JURIDICCIONALES ',car: 'ASISTENTE TECNICO UPJ DAJ'}];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.de.push({name: value,dep: value,car: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  add2(event2: MatChipInputEvent): void {
    const value = (event2.value || '').trim();

    // Add our fruit
    if (value) {
      this.para.push({name: value,dep: value,car: value});
    }

    // Clear the input value
    event2.chipInput!.clear();
  }

  remove(des: any): void {
    const index = this.de.indexOf(des);
    

    if (index >= 0) {
      this.de.splice(index, 1);
     
    }
  }

  remove2(paras: any): void {
   
    const index = this.para.indexOf(paras);

    if (index >= 0) {
  
      this.para.splice(index, 1);
    }
  }


  copias() {
    const dialogRef =  this.dialog.open(CrearCopyComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);  
      this.fruits=[...this.control.copiasNombres];
        console.log(this.fruits); 
       
    });
    
  }

  limpiarCopias(nombre: any){
    
    const indice = this.fruits.indexOf(nombre);
    if (indice !== -1) {
      this.fruits.splice(indice, 1);
    }

    console.log(nombre);
  }

  limpiarCopias2(nombre2: any){
    
    const indice = this.fruitslk.indexOf(nombre2);
    if (indice !== -1) {
      this.fruitslk.splice(indice, 1);
    }

    console.log(nombre2);
  }

  //del fruitscxczxczxcx

  add3(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.fruitCtrl.setValue(null);
  }

  remove3(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  displayFn(nombres: fruit ): string {
    return nombres && nombres.name ? nombres.name : '';
  }

  private _filter(name: string):fruit[] {
    const filterValue = name.toLowerCase();

    return this.allFruits.filter(allFruits => allFruits.name.toLowerCase().includes(filterValue));
  }

  
  borrador(){
    const tramites: tramite = {
      nro_tramite: this.form.value.nro_tramite,
      nro_copia: this.form.value.nro_copia,
      asunto: this.form.value.asunto,
      borrador: this.form.value.borrador,
      tipo_tramite: this.form.value.tipo_tramite,
      procedencia: this.form.value.procedencia,
      fecha_envio: this.form.value.fecha_envio,
      tomado_por: this.form.value.tomado_por,
      so_sistema_nombre: this.form.value.so_sistema_nombre,
      dias_atencion: this.form.value.dias_atencion,
      prioridad: this.form.value.prioridad,
    };
    //this.router.navigate(['/tramite/bandeja-tabs']);
    this.dialogRef.close();
    this._tramiteService2.agregarTramite(tramites);
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  agregar(){
    
    const tramites: tramite = {
      nro_tramite: this.form.value.nro_tramite,
      nro_copia: this.form.value.nro_copia,
      asunto: this.form.value.asunto,
      borrador: '',
      tipo_tramite: this.form.value.tipo_tramite,
      procedencia: this.form.value.procedencia,
      fecha_envio: this.form.value.fecha_envio,
      tomado_por: this.form.value.tomado_por,
      so_sistema_nombre: this.form.value.so_sistema_nombre,
      dias_atencion: this.form.value.dias_atencion,
      prioridad: this.form.value.prioridad,
    };
    //this.router.navigate(['/tramite/bandeja-tabs']);
    this.dialogRef.close();
    this._tramiteService.agregarTramite(tramites);
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
    this._snackbar.open('Tramite Enviado Exitosamente','',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
      });
  }

  openFusionar(): void {
    const dialogRef =  this.dialog.open(FusionComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (this.isButtonClicked) {
        // Acción para el segundo clic
        console.log('Segundo clic');
  
        this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
      } else {
        // Acción para el primer clic
        console.log('Primer clic');
        // Realiza la lógica deseada para el primer clic
   
        this.buttonLabel = 'Acción 2'; // Cambia la etiqueta del botón para indicar la próxima acción en el segundo clic
        this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
      }
    
    });
  }

  buttonClicked(): void {
    if (this.isButtonClicked) {
      // Acción para el segundo clic
      console.log('Segundo clic');
      
      // const currentUrl = this.router.url;
      // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      // this.router.onSameUrlNavigation = 'reload';
      // this.router.navigate([currentUrl]);
      //   this._snackBar.open('Tramite Enviado Exitosamente','',{
      //     duration:1500,
      //     horizontalPosition:'center',
      //     verticalPosition:'bottom'
      //     });
      
      this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
    } else {
      // Acción para el primer clic
      console.log('Primer clic');
      // Realiza la lógica deseada para el primer clic
      this.buttonLabel = 'Acción 2'; // Cambia la etiqueta del botón para indicar la próxima acción en el segundo clic
      this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
      this.isInputVisible = !this.isInputVisible; 
  }  
  }
}
