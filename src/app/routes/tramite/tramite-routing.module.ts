import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BandejaTabsComponent } from './bandeja-tabs/bandeja-tabs.component';
import { TramitesEntrantesComponent } from './tramites-entrantes/tramites-entrantes.component';
import { TramitesRecibidosComponent } from './tramites-recibidos/tramites-recibidos.component';
import { TramitesRevertidosComponent } from './tramites-revertidos/tramites-revertidos.component';
import { TramitesCreadosComponent } from './tramites-creados/tramites-creados.component';
import { TramitesEnviadosComponent } from './tramites-enviados/tramites-enviados.component';
import { ArchivadosComponent } from './archivados/archivados.component';
import { CrearComponent } from './crear/crear.component';
import { ConsultasComponent } from './consultas/consultas.component';
import { ReportesComponent } from './reportes/reportes.component';


const routes: Routes = [
  { path: 'bandeja-tabs', component: BandejaTabsComponent},
  { path: 'tramites-entrantes', component: TramitesEntrantesComponent},
  { path: 'tramites-recibidos', component: TramitesRecibidosComponent},
  { path: 'tramites-revertidos', component: TramitesRevertidosComponent},
  { path: 'tramites-creados', component: TramitesCreadosComponent},
  { path: 'tramites-enviados', component: TramitesEnviadosComponent},
  { path: 'archivados', component: ArchivadosComponent},
  { path: 'crear', component: CrearComponent},
  { path: 'consultas', component: ConsultasComponent},
  { path: 'reportes', component: ReportesComponent},
  


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TramiteRoutingModule { }
