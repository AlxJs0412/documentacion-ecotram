import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesEntrantesComponent } from './tramites-entrantes.component';

describe('TramitesEntrantesComponent', () => {
  let component: TramitesEntrantesComponent;
  let fixture: ComponentFixture<TramitesEntrantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesEntrantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramitesEntrantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
