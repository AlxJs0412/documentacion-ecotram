import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialEntrantesComponent } from './historial-entrantes.component';

describe('HistorialEntrantesComponent', () => {
  let component: HistorialEntrantesComponent;
  let fixture: ComponentFixture<HistorialEntrantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialEntrantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistorialEntrantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
