import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { TramitesService } from 'app/services/tramites.service';
import { MatTabGroup } from '@angular/material/tabs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import {HistorialEntrantesComponent } from './historial-entrantes/historial-entrantes.component';
import { TramiteEntranteService } from 'app/services/tramite-entrante.service';
import { TramiteModule } from '../tramite.module';
import { Router } from '@angular/router';
import { HistorialComponent } from '../tramites-recibidos/historial/historial.component';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';

interface Food {
  value: string;
  viewValue: string;
}


export interface tramite {
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}


@Component({
  selector: 'app-tramites-entrantes',
  templateUrl: './tramites-entrantes.component.html',
  styleUrls: ['./tramites-entrantes.component.scss']
})
export class TramitesEntrantesComponent {
  lisTramite:tramite[]=[];
  datos: any;
  displayedColumns: string[] = ['select','nuevo','nro_tramite','nro_copia','cite','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad','opciones'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
   dataSource: MatTableDataSource < any > = new MatTableDataSource < any > ([]);

  @ViewChild(MatSort) sort = new MatSort();
  constructor(private prueba: TramiteModule,private svRecibidos: TramiteRecibidoService,private router : Router,private _tramiteService: TramiteEntranteService, private _snackbar:MatSnackBar, private matTabGroup:MatTabGroup,private dialog: MatDialog, private recibir: TramiteModule){}
  selection = new SelectionModel<tramite>(true, []);
  ////combo
  selectedValue: string | undefined;

  accionesBloque: Food[] = [
    {value: 'recibir', viewValue: 'Recibir todos'},
  ];

  deleteElement(element: any) {
    const index = this.dataSource.data.indexOf(element);
    if (index >= 0) {


      const deletedElement = this.dataSource.data.splice(index, 1)[0]; // Elimina el elemento y obténlo
      this.recibir.entranteRecibido.unshift(deletedElement); // Agrega el elemento al arreglo de elementos eliminados
      this.dataSource.data = [...this.dataSource.data]; // Actualiza la referencia del arreglo de datos en la vista
    }
  }

  /// navegar de una pestaña tabs a otra
  seleccionarPestana(indice: number, element: any) {
    this.deleteElement(element)

    this._snackbar.open('Tramite Recibido Exitosamente','',{
      duration:1500,
      horizontalPosition:'end',
      verticalPosition:'bottom',
    });
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
    this.cargarTramite();
    this.matTabGroup.selectedIndex = indice;
    this.svRecibidos.recibido = true;
  }
  intervalo :  any
  ngOnInit(): void{
    this.cargarTramite();
    this.dataSource.data = this.dataSource.data.filter(element=>!this.recibir.entranteRecibido.includes(element));
    this.dataSource.data = this.dataSource.data.filter(element=>!this.recibir.tramiteN.includes(element));
    if (this.svRecibidos.recibido === true){
      this.matTabGroup.selectedIndex = 1;
      this.svRecibidos.recibido = false
    } if (this.svRecibidos.revertido === true){
      this.matTabGroup.selectedIndex = 2;
      this.svRecibidos.revertido = false
    } if (this.svRecibidos.creado === true){
      this.matTabGroup.selectedIndex = 3;
      this.svRecibidos.creado = false
    } if (this.svRecibidos.enviado === true){
      this.matTabGroup.selectedIndex = 4;
      this.svRecibidos.enviado = false
    } if (this.svRecibidos.entrante === true){
      this.matTabGroup.selectedIndex = 0;
      this.svRecibidos.entrante = false
    }
    this.resp=[...this.dataSource.data]
    this.intervalo = setInterval(() => {
      this.filtrarGeneral();
    }, 1000);

  }
  filtrarGeneral(): void {
    this.dataSource.filter = this.svRecibidos.filtrado.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  cargarTramite(){
    this.lisTramite=this._tramiteService.getTramite();
    this.dataSource = new MatTableDataSource(this.lisTramite);
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
  }
  }

  ////
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    console.log('ingreso',this.isAllSelected());
    if (this.isAllSelected()) {
      this.selection.clear(); ///deselecciona el checkk el listado si la respuesta es true
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

/** The label for the checkbox on the passed row */
checkboxLabel(row?: tramite): string {
  if (!row) {
    return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
  }
  return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
}

eliminarTramite(index:any,dataTram:any){
  console.log(index,dataTram);
  const dialogConfig = new MatDialogConfig();
  dialogConfig.data = { nrotramite: dataTram.nro_tramite,nrocopia:dataTram.nro_copia,asunto:dataTram.asunto };

  const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(result => {
    if (result === true) {
      // Realizar la eliminación del registro aquí
      this._tramiteService.eliminarTramite(index);
      this.cargarTramite();
      this._snackbar.open('Tramite Revertido Exitosamente','',{
            duration:1500,
            horizontalPosition:'center',
            verticalPosition:'bottom'
      });
    }

  });
}
openDialog(){
  console.log('openDialog');
  const dialogRef = this.dialog.open(HistorialComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  formatDate(date: Date): string {
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    } as Intl.DateTimeFormatOptions;
    const formattedDate = date.toLocaleString('en-US', options);
    const [datePart, timePart] = formattedDate.split(', ');
  
    const [month, day, year] = datePart.split('/');
    return `${year}-${month}-${day}`;
  }
  
  
  
   diaSeleccionado: string = 'Todas las fechas';

  resp : tramite[] = []
  
  selectDias(dias:string) {

    const primero: string []= []
    const segundo: string []= []
    const tercero: string []= []
  
    const fechaActual = new Date();
    
    
    for (let i =1 ; i<=21;i++){
      const fecha7DiasAntes = new Date();
      const fecha14DiasAntes = new Date();
      const fecha21DiasAntes = new Date();
      if (i<=7){
        
        fecha7DiasAntes.setDate(fechaActual.getDate() - i);
        primero.unshift(this.formatDate(fecha7DiasAntes))
      }else if(i<=14 && i>7){
        fecha14DiasAntes.setDate(fechaActual.getDate() - i);
        segundo.unshift(this.formatDate(fecha14DiasAntes))
        
      }else if(i>14 && i<=21){
        fecha21DiasAntes.setDate(fechaActual.getDate() - i);
        tercero.unshift(this.formatDate(fecha21DiasAntes))
        
      }
      
    }
  
    if (dias === "7") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${primero.join("|")}).*`);
      console.log('7');
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      this.diaSeleccionado="Últimos 7 días"
      
    } else if (dias === "14") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${segundo.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('14');
      this.diaSeleccionado="Últimos 14 días"
    } else if (dias === "21") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${tercero.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('21');
      this.diaSeleccionado="Últimos 21 días"
    } else if (dias === "todos"){
      this.dataSource.data = [...this.resp]
      this.diaSeleccionado="Todas las fechas"
    }
  }

  getSelectedRows() {

    this.dataSource.data.forEach((element) => {
      if (this.selection.isSelected(element)) {
        this.svRecibidos.tramitesEnvioBloque.push(element);
      }
    });
  
    console.log(this.svRecibidos.tramitesEnvioBloque)
  }

  recibirTodo(){
    this.svRecibidos.tramitesEnvioBloque= []
    this.getSelectedRows()
  
        this.prueba.entranteRecibido.unshift(...this.svRecibidos.tramitesEnvioBloque)
          this.dataSource.data = this.dataSource.data.filter((elemento) => !this.svRecibidos.tramitesEnvioBloque.includes(elemento));
          const currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
          this.cargarTramite();
        this.svRecibidos.controlBloque=false;
        this.svRecibidos.recibido=true;
        this.cargarTramite();
        const a : number = this.prueba.entranteRecibido.length
        this._snackbar.open(a+' Tramites Recibidos Exitosamente','',{
          duration:1500,
          horizontalPosition:'end',
          verticalPosition:'bottom',
        });
  }

  contador: number = 0;
  actualizarContador(checked: boolean) {
    if (checked) {
      this.contador++;
    }else {
      this.contador--;
    }
  }
  
  resetCont(checked: boolean){
    if(checked){
      this.contador = 2;
    }else{
      this.contador = 0;
    }
  }
}


@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html',
})
export class historialEntrantesComponent {}
