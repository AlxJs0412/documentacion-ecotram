import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesRecibidosComponent } from './tramites-recibidos.component';

describe('TramitesRecibidosComponent', () => {
  let component: TramitesRecibidosComponent;
  let fixture: ComponentFixture<TramitesRecibidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesRecibidosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramitesRecibidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
