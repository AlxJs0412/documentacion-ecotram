import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionarComponent } from './fusionar.component';

describe('FusionarComponent', () => {
  let component: FusionarComponent;
  let fixture: ComponentFixture<FusionarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FusionarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FusionarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
