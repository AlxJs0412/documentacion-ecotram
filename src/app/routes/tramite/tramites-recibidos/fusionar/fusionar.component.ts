import {SelectionModel} from '@angular/cdk/collections';
import {Component} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';

@Component({
  selector: 'app-fusionar',
  templateUrl: './fusionar.component.html',
  styleUrls: ['./fusionar.component.scss']
})
export class FusionarComponent {

  constructor(private svRecibido: TramiteRecibidoService){  }
  
  NODO_PRINCIPAL : NodoPadre [] = [...this.svRecibido.nodoPadre]
  NODO_PADRE : NodoPadre [] = [...this.svRecibido.other]
  NODO_HIJO : NodoHijo [] = [...this.svRecibido.nodoHijo.filter(element=>!this.svRecibido.other.includes(element))]


  customColor: boolean = true;

  displayedColumnsPrincipal: string[] = ['tramite', 'copias', 'asunto', 'tipo','procedencia', 'envio','remitente','prioridad'];
  dataSourcePrincipal = new MatTableDataSource<NodoPadre>(this.NODO_PRINCIPAL);

  displayedColumnsPadre: string[] = ['select','tramite', 'copias', 'asunto', 'tipo','procedencia', 'envio','remitente','prioridad'];
  dataSourcePadre = new MatTableDataSource<NodoPadre>(this.NODO_PADRE);
  selectionPadre = new SelectionModel<NodoPadre>(true, []);

  displayedColumnsHijo: string[] = ['select','tramite', 'copias', 'asunto', 'tipo','procedencia', 'envio','remitente','prioridad'];
  dataSourceHijo = new MatTableDataSource<NodoHijo>(this.NODO_HIJO);
  selectionHijo = new SelectionModel<NodoHijo>(true, []);

  getSelectedRows(selection: SelectionModel<NodoHijo>, data: NodoHijo[]): NodoHijo[] {
    
    const selectedRows: NodoHijo[] = [];
    selectedRows.splice(0, selectedRows.length);
  
    data.forEach((element) => {
      if (selection.isSelected(element)) {
        selectedRows.push(element);
      }
    });
    const selectedRowsPadre: NodoPadre[] = selectedRows.map((element) => ({

      nro_tramite: element.nro_tramite,
      nro_copia: element.nro_copia,
      asunto: element.asunto,
      tipo_tramite: element.tipo_tramite,
      procedencia: element.procedencia,
      fecha_envio: element.fecha_envio,
      tomado_por: element.tomado_por,
      so_sistema_nombre: element.so_sistema_nombre,
      dias_atencion: element.dias_atencion,
      prioridad: element.prioridad

    }));
    this.dataSourcePadre.data = [...this.dataSourcePadre.data, ...selectedRowsPadre];
    this.svRecibido.other = [...selectedRows]
     // Eliminar las filas seleccionadas de la tabla hijo
    selectedRows.forEach((element) => {
      const index = this.dataSourceHijo.data.indexOf(element);
      if (index > -1) {
        this.dataSourceHijo.data.splice(index, 1);
      }
    });

    // Actualizar el dataSourceHijo para refrescar la tabla
    this.dataSourceHijo.data = this.dataSourceHijo.data.slice();  

    // Vaciar el array selectedRows para la próxima ejecución
    // selectedRows.splice(0, selectedRows.length);

    return selectedRows;
    
  }
  
  moverElementos() {
    const elementosSeleccionados = this.selectionPadre.selected;
  
    if (elementosSeleccionados.length > 0) {
      const elementosNoSeleccionados = this.dataSourcePadre.data.filter((element, index) => index !== 0 && !this.selectionPadre.isSelected(element));
  
      this.dataSourceHijo.data = [...this.dataSourceHijo.data, ...elementosSeleccionados];
      this.dataSourcePadre.data = [...elementosNoSeleccionados];
      this.selectionPadre.clear(); // Limpiar la selección después de mover los elementos
    }
  }
  
  
  
  

  
  verLog(){
    const selectedRows2 = this.getSelectedRows(this.selectionHijo, this.NODO_HIJO);
    console.log(selectedRows2);
  }
  


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelectedPadre() {
    const numSelected = this.selectionPadre.selected.length;
    const numRows = this.dataSourcePadre.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRowsPadre() {
    if (this.isAllSelectedPadre()) {
      this.selectionPadre.clear();
      return;
    }

    this.selectionPadre.select(...this.dataSourcePadre.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabelPadre(row?: NodoPadre): string {
    if (!row) {
      return `${this.isAllSelectedPadre() ? 'deselect' : 'select'} all`;
    }
    return `${this.selectionPadre.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
  }

  isAllSelectedHijo() {
    const numSelected = this.selectionHijo.selected.length;
    const numRows = this.dataSourceHijo.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRowsHijo() {
    if (this.isAllSelectedHijo()) {
      this.selectionHijo.clear();
      return;
    }

    this.selectionHijo.select(...this.dataSourceHijo.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabelHijo(row?: NodoHijo): string {
    if (!row) {
      return `${this.isAllSelectedHijo() ? 'deselect' : 'select'} all`;
    }
    return `${this.selectionPadre.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
  }



}





export interface NodoPadre{
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}

//const NODO_PADRE: NodoPadre[]=[]
// const NODO_PADRE: NodoPadre[] = [
//   { tramite: 26032, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL', procedencia: 'externo', envio:'2022 07 26\n17:31:42',remitente:'tvasquez', prioridad:'alta' },
// ]


export interface NodoHijo{
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}

// const NODO_HIJO: NodoHijo[] = [
//   { prioridad: 'baja', remitente:'vchoque', envio:'2022 07 26\n17:31:42', procedencia: 'interno', tramite: 10001, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 023/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' },
//   { prioridad: 'media', remitente:'vchoque', envio:'2022 07 26\n17:31:42', procedencia: 'interno', tramite: 10002, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' },
//   { prioridad: 'baja', remitente:'vchoque', envio:'2022 07 26\n17:31:42', procedencia: 'interno', tramite: 10003, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' },
//   { prioridad: 'baja', remitente:'vchoque', envio:'2022 07 26\n17:31:42', procedencia: 'interno', tramite: 10004, copias: 'ORIGINAL', asunto: 'SMFIN/DTM/UAO Nº 056/2022 RECAUDACIÓN MEDIANTE SERVICIOS DE PROCESAMIENTO TRANSACCIONAL POR INTERNET', tipo: 'CORRESPONDENCIA EJECUTIVO MUNICIPAL' },
// ]

// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];
