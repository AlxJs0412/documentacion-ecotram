import { Component } from '@angular/core';
import {  MatDialog, MatDialogRef } from '@angular/material/dialog';
import { BuscarPersonaComponent } from '../../buscar-persona/buscar-persona.component';
import { FusionarComponent } from '../fusionar/fusionar.component';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TramiteModule } from '../../tramite.module';
import { Router } from '@angular/router';
import { CrearCopiasComponent } from '../crear-copias/crear-copias.component';
import { BpRecibidosComponent } from '../bp-recibidos/bp-recibidos.component';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';


@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.component.html',
  styleUrls: ['./enviar.component.scss']
})
export class EnviarComponent {
  constructor(private personasCopia: TramiteRecibidoService,public copias: MatDialog,public buscarPersona: MatDialog, public fusionar: MatDialog, private _snackBar: MatSnackBar, public control : TramiteModule,private router: Router,public dialogRef: MatDialogRef<EnviarComponent>){}

  buttonLabel: string = 'Acción 1'; // Etiqueta del botón
  isButtonClicked: boolean = false; // Variable de control
  copiasControl: boolean = false;

  datosCopia=[...this.control.copiasNombres]
  datosPersona=[...this.personasCopia.persona]
  

  openCopias(){
    console.log('openDialog');
    const dialogRef = this.copias.open(CrearCopiasComponent);

      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.datosCopia=[...this.control.copiasNombres]
        console.log(this.datosCopia)
      });
    }

  limpiarCopias(nombre: any){

    const indice = this.datosCopia.indexOf(nombre);
    if (indice !== -1) {
      this.datosCopia.splice(indice, 1);
    }

    console.log(nombre)
  }

  mostrarCopias(){
    this.copiasControl=true;
  }

  enviarTramite(): void {
    this.control.control=true
    this.dialogRef.close()
  }

  buttonClicked(): void {
    if (this.isButtonClicked) {
      // Acción para el segundo clic
      console.log('Segundo clic');

      this.isButtonClicked = false; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió


      this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
    } else {
      // Acción para el primer clic
      console.log('Primer clic');
      // Realiza la lógica deseada para el primer clic
      this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
    }
  }

  openBP(){
    const dialogRef = this.buscarPersona.open(BpRecibidosComponent);
    
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.datosPersona=[...this.personasCopia.persona]
        this.personasCopia.persona=[]
      });
    }

   openFusionar(){

    const dialogRef = this.buscarPersona.open(FusionarComponent);
        dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        
      });
  }

  de = [
    {
      name: 'Oscar Martinez Ramos',
      dep: 'SEM',
      car: 'Asistente Legal',
      icon: 'account_circle'
    }
  ];

  para = [
    {
      name: 'leslie.santiago',
      dep: 'UNIDAD DE PROCESOS JURIDICCIONALES ',
      car: 'ASISTENTE TECNICO UPJ DAJ',
      icon: 'account_circle'
    }
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.de.push({name: value,dep: value,car: value, icon: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  add2(event2: MatChipInputEvent): void {
    const value = (event2.value || '').trim();

    // Add our fruit
    if (value) {
      this.para.push({name: value,dep: value,car: value,icon:value});
    }

    // Clear the input value
    event2.chipInput!.clear();
  }

  remove(des: any): void {
    const index = this.de.indexOf(des);


    if (index >= 0) {
      this.de.splice(index, 1);

    }
  }

  remove2(paras: any): void {

    const index = this.para.indexOf(paras);

    if (index >= 0) {

      this.para.splice(index, 1);
    }
  }


}
