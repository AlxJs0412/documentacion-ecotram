import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCopiasComponent } from './crear-copias.component';

describe('CrearCopiasComponent', () => {
  let component: CrearCopiasComponent;
  let fixture: ComponentFixture<CrearCopiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearCopiasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearCopiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
