import { Component, ViewChild } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort } from '@angular/material/sort';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';
import { BpRecibidosComponent } from '../bp-recibidos/bp-recibidos.component';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { FusionarComponent } from '../fusionar/fusionar.component';


@Component({
  selector: 'app-enviar-todos',
  templateUrl: './enviar-todos.component.html',
  styleUrls: ['./enviar-todos.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class EnviarTodosComponent {

  constructor(private svTramiteRecibido: TramiteRecibidoService, private buscarPersona: MatDialog, private fusionar : MatDialog){}

  mostrarProv = false;

  mostrarProveido(event: MatCheckboxChange) {
    this.mostrarProv = event.checked;
  }
  mostrarAdj = false;

  mostrarAdjunto(event: MatCheckboxChange) {
    this.mostrarAdj = event.checked;
  }

  columnsToDisplayWithExpand = ['nro_tramite','nro_copia','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad', 'expand'];
  expandedElement = null;

  lisTramite: tramite[] = [...this.svTramiteRecibido.tramitesEnvioBloque];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // dataSource: MatTableDataSource < any > = new MatTableDataSource < any > ([]);
  dataSource=new  MatTableDataSource(this.lisTramite);
  @ViewChild(MatSort) sort = new MatSort();
  selection = new SelectionModel<tramite>(true, []);
  ////combo
  selectedValue: string | undefined;

  datosPersona=[...this.svTramiteRecibido.persona]

  enviarTodo(){
    this.svTramiteRecibido.controlBloque =true

  }

  openFusionar(){
    const dialogRef = this.buscarPersona.open(FusionarComponent);
   
      dialogRef.afterClosed().subscribe(result => {
        
      });
  }

  openBP(){
    const dialogRef = this.buscarPersona.open(BpRecibidosComponent);
    this.svTramiteRecibido.persona=[]
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.datosPersona=[...this.svTramiteRecibido.persona]
        this.svTramiteRecibido.persona=[]
      });
    }

    add2(event2: MatChipInputEvent): void {
      const value = (event2.value || '').trim();
  
      // Add our fruit
      if (value) {
        this.para.push({name: value,dep: value,car: value,icon:value});
      }
  
      // Clear the input value
      event2.chipInput!.clear();
    }

    para = [
      {
        name: 'leslie.santiago',
        dep: 'UNIDAD DE PROCESOS JURIDICCIONALES ',
        car: 'ASISTENTE TECNICO UPJ DAJ',
        icon: 'account_circle'
      }
    ];

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    console.log('ingreso',this.isAllSelected());
    if (this.isAllSelected()) {
      this.selection.clear(); ///deselecciona el checkk el listado si la respuesta es true
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

/** The label for the checkbox on the passed row */
checkboxLabel(row?: tramite): string {
  if (!row) {
    return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
  }
  return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
}


selectedRows: any[] = [];

selectRow(element: any) {
  const index = this.selectedRows.indexOf(element);
  if (index === -1) {
    // Agregar el elemento a la lista de elementos seleccionados
    this.selectedRows.push(element);
  } else {
    // Eliminar el elemento de la lista de elementos seleccionados
    console.log('AlxJs')
  }
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
}
}
isSelected2(element: any): boolean {
  return this.selectedRows.includes(element);
}

}

export interface tramite {
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}
