import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviarTodosComponent } from './enviar-todos.component';

describe('EnviarTodosComponent', () => {
  let component: EnviarTodosComponent;
  let fixture: ComponentFixture<EnviarTodosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnviarTodosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EnviarTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
