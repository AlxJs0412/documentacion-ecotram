import { Component } from '@angular/core';
import { TramiteModule } from '../../tramite.module';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';

@Component({
  selector: 'app-bp-recibidos',
  templateUrl: './bp-recibidos.component.html',
  styleUrls: ['./bp-recibidos.component.scss']
})
export class BpRecibidosComponent {
 
  constructor(public copiaVec: TramiteModule, public svRecibidos : TramiteRecibidoService){}

  selectedRow: any;

  seleccionado: string ="0";

  seleccionadoCant(){
    if (this.selectedRow===""){
      this.seleccionado = "0"
    }else{
      this.seleccionado = "1"
    }
  }

  obtenerElemento(element: any){
    this.svRecibidos.persona=[]
    this.svRecibidos.persona.push(element)
    console.log(element)
  }
  deseleccionarTodo(){
    this.selectedRow=""
    this.seleccionado = "0"
  }

  displayedColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  dataSourceDGRH = new MatTableDataSource<PeriodicElement>(DGRH);
  selectionDGRH = new SelectionModel<PeriodicElement>(true, []);

  dataSourceDA = new MatTableDataSource<PeriodicElement>(DA);
  selectionDA = new SelectionModel<PeriodicElement>(true, []);

  dataSourceAMC = new MatTableDataSource<PeriodicElement>(AMC);
  selectionAMC = new SelectionModel<PeriodicElement>(true, []);

  dataSourceDLC = new MatTableDataSource<PeriodicElement>(DLC);
  selectionDLC = new SelectionModel<PeriodicElement>(true, []);

  dataSourceDEE = new MatTableDataSource<PeriodicElement>(DEE);
  selectionDEE = new SelectionModel<PeriodicElement>(true, []);

  

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  vistaArea:string='DTIGA'

  cambiarVista(cambio: string){
    this.vistaArea=cambio;
    console.log(this.vistaArea)
  }

  nombresCopias() {
    const elementosSeleccionados = this.selection.selected;
  
    if (elementosSeleccionados.length > 0) {
      const elementosNoSeleccionados = this.dataSource.data.filter((element, index) => index !== 0 && !this.selection.isSelected(element));

      this.copiaVec.copiasNombres = [...elementosSeleccionados] 
  
      // Guardar los elementos seleccionados en otro array
      //const elementosGuardados = [...elementosSeleccionados];
  
      // Limpiar la selección después de guardar los elementos
      this.selection.clear();
  
      // Continuar con la lógica de mover los elementos no seleccionados
      // this.dataSourcePadre.data = [this.dataSourcePadre.data[0], ...elementosNoSeleccionados];

      console.log(this.copiaVec.copiasNombres)
    }
  }

}

export interface PeriodicElement {
  position: number;
  name: string;
  direccion: string;
  cargo: string;
  correo:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'DTIGA',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'DTIGA',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'DTIGA',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'DTIGA',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'DTIGA',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'},
];

const DGRH: PeriodicElement[]=[
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'DGRH',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'DGRH',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'DGRH',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'DGRH',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'DGRH',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'}
];

const DA: PeriodicElement[]=[
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'DA',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'DA',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'DA',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'DA',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'DA',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'}
]

const AMC: PeriodicElement[]=[
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'AMC',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'AMC',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'AMC',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'AMC',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'AMC',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'}
]

const DLC: PeriodicElement[]=[
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'DLC',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'DLC',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'DLC',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'DLC',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'DLC',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'}
]

const DEE: PeriodicElement[]=[
  {position:1,correo:'oscar.ramos@lapaz.bo',direccion:'DEE',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:2,correo:'oscar.ramos@lapaz.bo',direccion:'DEE',cargo:'Asistente Legal', name: 'Oscar Martinez Ramos'},
  {position:3,correo:'oscar.ramos@lapaz.bo',direccion:'DEE',cargo:'Asistente Legal', name: 'Roman Ramos Perez'},
  {position:4,correo:'oscar.ramos@lapaz.bo',direccion:'DEE',cargo:'Asistente Legal', name: 'Silvia Rocabado Ramos'},
  {position:5,correo:'oscar.ramos@lapaz.bo',direccion:'DEE',cargo:'Asistente Legal', name: 'Margarita Martinez Ramos'}
]