import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BpRecibidosComponent } from './bp-recibidos.component';

describe('BpRecibidosComponent', () => {
  let component: BpRecibidosComponent;
  let fixture: ComponentFixture<BpRecibidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BpRecibidosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BpRecibidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
