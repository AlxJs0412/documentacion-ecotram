import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TramiteRoutingModule } from './tramite-routing.module';

import { TramitesEntrantesComponent } from './tramites-entrantes/tramites-entrantes.component';
import { TramitesRecibidosComponent } from './tramites-recibidos/tramites-recibidos.component';
import { TramitesRevertidosComponent } from './tramites-revertidos/tramites-revertidos.component';
import { TramitesEnviadosComponent } from './tramites-enviados/tramites-enviados.component';
import { BandejaTabsComponent } from './bandeja-tabs/bandeja-tabs.component';
import { SharedModule } from '@shared/shared.module';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { CardEtrComponent } from './card-etr/card-etr.component';
import { CreateDialogComponent } from './create-dialog/create-dialog.component';

import { ArchivadosComponent } from './archivados/archivados.component';
import { HistorialComponent } from './tramites-recibidos/historial/historial.component';
import { CrearComponent } from './crear/crear.component';
import { BuscarPersonaComponent } from './buscar-persona/buscar-persona.component';
import { HistorialRevertidosComponent } from './tramites-revertidos/historial-revertidos/historial-revertidos.component';
import { HistorialEntrantesComponent } from './tramites-entrantes/historial-entrantes/historial-entrantes.component';
import { HistorialEnviadosComponent } from './tramites-enviados/historial-enviados/historial-enviados.component';
import { ConsultasComponent } from './consultas/consultas.component';
import { ReportesComponent } from './reportes/reportes.component';

import { NuevoTramiteComponent } from './tramites-creados/nuevo-tramite/nuevo-tramite.component';
import { Confirm2DialogComponent } from './tramites-creados/confirm2-dialog/confirm2-dialog.component';
import { EnviarComponent } from './tramites-recibidos/enviar/enviar.component';
import { EnviarComponentRev } from './tramites-revertidos/enviar/enviar.component';
import { FusionarComponent } from './tramites-recibidos/fusionar/fusionar.component';
import { TramiteNuevoComponent } from './tramites-creados/tramite-nuevo/tramite-nuevo.component';

import { ConfirmaDialogComponent } from './tramites-enviados/confirma-dialog/confirma-dialog.component';
import { CrearCopiasComponent } from './tramites-recibidos/crear-copias/crear-copias.component';
import { CrearCopyComponent } from './tramites-creados/crear-copy/crear-copy.component';
import { CrearCopy2Component } from './tramites-creados/crear-copy2/crear-copy2.component';
import { Buscarper2Component } from './tramites-creados/buscarper2/buscarper2.component';
import { BuscarperComponent } from './tramites-creados/buscarper/buscarper.component';
import { BpRecibidosComponent } from './tramites-recibidos/bp-recibidos/bp-recibidos.component';
import { FusionComponent } from './tramites-creados/fusion/fusion.component';
import { Fusion2Component } from './tramites-creados/fusion2/fusion2.component';
import { TramitesCreadosComponent } from './tramites-creados/tramites-creados.component';
import { EnviarTodosComponent } from './tramites-recibidos/enviar-todos/enviar-todos.component';



@NgModule({
  declarations: [
    TramitesCreadosComponent,
    TramitesEntrantesComponent,
    TramitesRecibidosComponent,
    TramitesRevertidosComponent,
    TramitesEnviadosComponent,
    BandejaTabsComponent,
    ConfirmDialogComponent,
    ArchivadosComponent,
    CardEtrComponent,
    CreateDialogComponent,
    HistorialComponent,
    CrearComponent,
    BuscarPersonaComponent,
    HistorialComponent,
    HistorialRevertidosComponent,
    HistorialEntrantesComponent,
    HistorialEnviadosComponent,
    ConsultasComponent,
    ReportesComponent,
    NuevoTramiteComponent,
    Confirm2DialogComponent,
    EnviarComponent,
    FusionarComponent,
    TramiteNuevoComponent,
    FusionComponent,
    Fusion2Component,
    EnviarComponentRev,
    ConfirmaDialogComponent,
    CrearCopiasComponent,
    CrearCopyComponent,
    CrearCopy2Component,
    Buscarper2Component,
    BuscarperComponent,
    BpRecibidosComponent,
    EnviarTodosComponent

  ],
  imports: [
    SharedModule,
    CommonModule,
    TramiteRoutingModule
  ]
})
export class TramiteModule {
  public tramiteN :any[]=[];
  public control = false;

  //tranferir tramites

  public entranteRecibido : any [] = [];
  public enviadoRecibido : any [] = [];
  public entranteRevertido : any [] = [];

  public copiasNombres: any [] = [];
  public aaa: any [] = [];

}
