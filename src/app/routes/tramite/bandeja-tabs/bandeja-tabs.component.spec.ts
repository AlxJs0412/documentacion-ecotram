import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BandejaTabsComponent } from './bandeja-tabs.component';

describe('BandejaTabsComponent', () => {
  let component: BandejaTabsComponent;
  let fixture: ComponentFixture<BandejaTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BandejaTabsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BandejaTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
