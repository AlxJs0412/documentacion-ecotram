import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesRevertidosComponent } from './tramites-revertidos.component';

describe('TramitesRevertidosComponent', () => {
  let component: TramitesRevertidosComponent;
  let fixture: ComponentFixture<TramitesRevertidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TramitesRevertidosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TramitesRevertidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
