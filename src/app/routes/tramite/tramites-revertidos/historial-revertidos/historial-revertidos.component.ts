import { Component } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-historial-revertidos',
  templateUrl: './historial-revertidos.component.html',
  styleUrls: ['./historial-revertidos.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class HistorialRevertidosComponent {
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  // dataSource = ELEMENT_DATA;

  dataSource2 = ELEMENT_DATA;
  columnsToDisplay2 = ['name', 'weight', 'symbol', 'position','other'];

  columnsToDisplay: string[] = [
  'sendBy',
  'Node',
  'dateRecep',
  'recepBy',
  'fojas',
  'description',
  'priority'
]
  dataSource = TABLE_DATA;
  columnsToDisplayWithExpand = ['dateSend','sendBy',
  'Node',
  'dateRecep',
  'recepBy',
  'fojas',
  'description',
  'priority', 'expand'];
  expandedElement: PeriodicElement | null = null;

  tramite_tp: tramite={nro: '26312 - Original', tipo: 'CORRESPONDENCIA CIUDADANA', asunto:' DANIEL MARTIN ARIAS - CERTIFICACIÓN PARA REEMPLAQUE'}
  formatDate(date: Date, time: boolean): string {
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    } as Intl.DateTimeFormatOptions;
    const formattedDate = date.toLocaleString('en-US', options);
    const [datePart, timePart] = formattedDate.split(', ');
    if (time == false){
      return datePart
    }else{
      return timePart
    }
  }


}

// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }



// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];

export interface TableData{
  dateSend: Date;
  sendBy: string;
  Node: string;
  dateRecep: Date;
  recepBy: string;
  fojas: Number;
  description: string;
  priority: string;

}

const TABLE_DATA: TableData[]=[
  {dateSend: new Date('2022-01-31T10:30:00Z'),sendBy:'iHuanca',Node:'PALTAFORMA DE ATENCION CIUDADANA EDIFICIO ARMANDO ESCOBAR URIA',dateRecep: new Date('2022-01-31T10:30:00Z'),recepBy:'iHuanca', fojas:12, description:'notas y copias', priority:'urgente'},
  {dateSend: new Date('2022-01-31T10:30:00Z'),sendBy:'iHuanca',Node:'ADMINISTRACION TRIBUTARIA MUNICIPAL',dateRecep: new Date('2022-01-31T10:30:00Z'),recepBy:'iHuanca', fojas:12, description:'notas y copias', priority:'baja'},
  {dateSend: new Date('2022-01-31T10:30:00Z'),sendBy:'iHuanca',Node:'PALTAFORMA DE ATENCION CIUDADANA EDIFICIO ARMANDO ESCOBAR URIA',dateRecep: new Date('2022-01-31T10:30:00Z'),recepBy:'iHuanca', fojas:0, description:'notas y copias', priority:'urgente'},
  {dateSend: new Date('2022-01-31T10:30:00Z'),sendBy:'iHuanca',Node:'PALTAFORMA DE ATENCION CIUDADANA EDIFICIO ARMANDO ESCOBAR URIA',dateRecep: new Date('2022-01-31T10:30:00Z'),recepBy:'iHuanca', fojas:0, description:'notas y copias', priority:'urgente'},
  {dateSend: new Date('2022-01-31T10:30:00Z'),sendBy:'iHuanca',Node:'PALTAFORMA DE ATENCION CIUDADANA EDIFICIO ARMANDO ESCOBAR URIA',dateRecep: new Date('2022-01-31T10:30:00Z'),recepBy:'iHuanca', fojas:0, description:'notas y copias', priority:'media'},
]


export interface tramite{
  nro:string;
  tipo:string;
  asunto:string;
}




export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    position: 1,
    name: 'Hydrogen',
    weight: 1.0079,
    symbol: 'H',
    description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`,
  },
  {
    position: 2,
    name: 'Helium',
    weight: 4.0026,
    symbol: 'He',
    description: `Helium is a chemical element with symbol He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`,
  },
  {
    position: 3,
    name: 'Lithium',
    weight: 6.941,
    symbol: 'Li',
    description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`,
  },
  {
    position: 4,
    name: 'Beryllium',
    weight: 9.0122,
    symbol: 'Be',
    description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`,
  },
  {
    position: 5,
    name: 'Boron',
    weight: 10.811,
    symbol: 'B',
    description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`,
  },
  {
    position: 6,
    name: 'Carbon',
    weight: 12.0107,
    symbol: 'C',
    description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
        and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
        to group 14 of the periodic table.`,
  },
  {
    position: 7,
    name: 'Nitrogen',
    weight: 14.0067,
    symbol: 'N',
    description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
        discovered and isolated by Scottish physician Daniel Rutherford in 1772.`,
  },
  {
    position: 8,
    name: 'Oxygen',
    weight: 15.9994,
    symbol: 'O',
    description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
         the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
         agent that readily forms oxides with most elements as well as with other compounds.`,
  },
  {
    position: 9,
    name: 'Fluorine',
    weight: 18.9984,
    symbol: 'F',
    description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
        lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
        conditions.`,
  },
  {
    position: 10,
    name: 'Neon',
    weight: 20.1797,
    symbol: 'Ne',
    description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
        Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
        two-thirds the density of air.`,
  },
];



