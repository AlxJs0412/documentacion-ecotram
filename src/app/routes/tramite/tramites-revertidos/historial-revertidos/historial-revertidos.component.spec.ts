import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialRevertidosComponent } from './historial-revertidos.component';

describe('HistorialRevertidosComponent', () => {
  let component: HistorialRevertidosComponent;
  let fixture: ComponentFixture<HistorialRevertidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialRevertidosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistorialRevertidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
