import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material/dialog';
import { CrearComponent } from '../crear/crear.component';
import { HistorialRevertidosComponent } from './historial-revertidos/historial-revertidos.component';
import { EnviarComponentRev } from './enviar/enviar.component';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { BuscarPersonaComponent } from '../buscar-persona/buscar-persona.component';
import { TramiteRevertidoService } from 'app/services/tramite-revertido.service';
import { TramiteModule } from '../tramite.module';
import { Router } from '@angular/router';
import { HistorialComponent } from '../tramites-recibidos/historial/historial.component';
import { EnviarComponent } from '../tramites-recibidos/enviar/enviar.component';
import { TramiteRecibidoService } from 'app/services/tramite-recibido.service';
import { EnviarTodosComponent } from '../tramites-recibidos/enviar-todos/enviar-todos.component';

interface Food {
  value: string;
  viewValue: string;
}

export interface tramite {
  nro_tramite: number;
  nro_copia: string;
  asunto: string;
  tipo_tramite: string;
  procedencia: string;
  fecha_envio: string;
  tomado_por: string;
  so_sistema_nombre: string;
  dias_atencion: string;
  prioridad: string;
}
@Component({
  selector: 'app-tramites-revertidos',
  templateUrl: './tramites-revertidos.component.html',
  styleUrls: ['./tramites-revertidos.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TramitesRevertidosComponent implements OnInit {
private intervalo:any
  ngOnInit() {
    console.log(this.expandedElement);
    this.expandedElement = null; // Establecer expandedElement en null para colapsar todas las filas
    this.cargarTramite();
    this.dataSource.data = this.dataSource.data.filter(element=>!this.prueba.tramiteN.includes(element));
    console.log('sale el filtro')
    this.resp=[...this.dataSource.data]
    this.intervalo = setInterval(() => {
      this.filtrarGeneral();
    }, 1000);

  }
  filtrarGeneral(): void {
    this.dataSource.filter = this.svRecibidos.filtrado.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  lisTramite: tramite[] = [];
  //   {nro_tramite:1412,nro_copia:"ORIGINAL",asunto: "GAMLP - REQUERIMIENTO DE PAPEL MEMBRETADO",tipo_tramite:"	CORRESPONDENCIA EJECUTIVO MUNICIPAL",procedencia:"INTERNO",fecha_envio:"	2023-04-11 11:41:06",tomado_por:"",so_sistema_nombre:"ECOTRAM",dias_atencion:"",prioridad:"Urgente"},
  //   {nro_tramite:833,nro_copia:"ORIGINAL",asunto: "	SOLICITUD DE LICENCIA DE FUNCIONAMIENTO",tipo_tramite:"ORDEN DE DESPACHO",procedencia:"INTERNO",fecha_envio:"2023-04-06 17:47:51",tomado_por:"maritzabel.rosso",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:""},
  //   {nro_tramite:145,nro_copia:"ORIGINAL",asunto: "	LLANOS PEDRO - ACLATRACION SOL DE APERTURA DE GARAJE",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2023-04-06 14:30:13",tomado_por:"lizeth.marin",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Baja"},
  //   {nro_tramite:242,nro_copia:"1",asunto: "GAMLP/SMDE/ N. 019/23. - CRONOGRAMA DE OPERATIVOS USO DE MOVILIDADES",tipo_tramite:"	CORRESPONDENCIA EJECUTIVO MUNICIPAL",procedencia:"EXTERNO",fecha_envio:"2023-03-30 14:51:03",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Ata"},
  //   {nro_tramite:450,nro_copia:"ORIGINAL",asunto: "	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA",tipo_tramite:"CORRESPONDENCIA CIUDADANA",procedencia:"EXTERNO",fecha_envio:"2022-03-04 09:09:56",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Media"},
  //   {nro_tramite:760,nro_copia:"ORIGINAL",asunto: "ASOCIACION DE COMERCIANTES MINORISTAS EN TELAS - AUDIENCIA",tipo_tramite:"	COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS",procedencia:"EXTERNO",fecha_envio:"2023-03-27 15:09:54",tomado_por:"m.tellez",so_sistema_nombre:"ECOTRAM",dias_atencion:"3",prioridad:"Media"}
  // ];

  displayedColumns: string[] = ['select','nro_tramite','nro_copia','cite','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad','opciones'];

  columnsToDisplayWithExpand = ['select','nro_tramite','nro_copia','cite','asunto','tipo_tramite','procedencia','fecha_envio','tomado_por','so_sistema_nombre','dias_atencion','prioridad', 'expand'];
  expandedElement = null;



  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // dataSource: MatTableDataSource < any > = new MatTableDataSource < any > ([]);
  dataSource=new  MatTableDataSource(this.lisTramite);
  @ViewChild(MatSort) sort = new MatSort();
  selection = new SelectionModel<tramite>(true, []);
  ////combo
  selectedValue: string | undefined;

  accionesBloque: Food[] = [
    {value: 'recibir', viewValue: 'Recibir todos'},
  ];
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
  }
  }



  ////
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    console.log('ingreso',this.isAllSelected());
    if (this.isAllSelected()) {
      this.selection.clear(); ///deselecciona el checkk el listado si la respuesta es true
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

/** The label for the checkbox on the passed row */
checkboxLabel(row?: tramite): string {
  if (!row) {
    return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
  }
  return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.nro_tramite + 1}`;
}
constructor(private svRecibidos:TramiteRecibidoService,public dialog: MatDialog, public historial: MatDialog, private _tramiteService: TramiteRevertidoService, public prueba: TramiteModule,private router: Router) {}

deleteElement(element: any) {
  const index = this.dataSource.data.indexOf(element);
  if (index >= 0) {
    // this.dataSource.data.splice(index, 1);
    // this.dataSource.data = [...this.dataSource.data];

    const deletedElement = this.dataSource.data.splice(index, 1)[0]; // Elimina el elemento y obténlo
    this.prueba.tramiteN.unshift(deletedElement); // Agrega el elemento al arreglo de elementos eliminados
    this.dataSource.data = [...this.dataSource.data]; // Actualiza la referencia del arreglo de datos en la vista
  }
}

openDialog(elm : any){
  console.log('openDialog');
  const dialogRef = this.dialog.open(EnviarComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if(this.prueba.control==true){
        this.deleteElement(elm)
        const currentUrl = this.router.url;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);
        this.cargarTramite();
      }
    });
  }

  cargarTramite(){
    this.lisTramite=this._tramiteService.getTramite();
    this.dataSource = new MatTableDataSource(this.lisTramite);
  }



openHistorial(){
  console.log('openDialog');
  const dialogRef = this.historial.open(HistorialComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


  openBuscarPersona(){
    console.log('aaaa');
    const dialogRef = this.historial.open(BuscarPersonaComponent);

      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
  }

  formatDate(date: Date): string {
    const options = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    } as Intl.DateTimeFormatOptions;
    const formattedDate = date.toLocaleString('en-US', options);
    const [datePart, timePart] = formattedDate.split(', ');
  
    const [month, day, year] = datePart.split('/');
    return `${year}-${month}-${day}`;
  }
  
  
  
   diaSeleccionado: string = 'Todas las fechas';

  resp : tramite[] = []
  
  selectDias(dias:string) {

    const primero: string []= []
    const segundo: string []= []
    const tercero: string []= []
  
    const fechaActual = new Date();
    
    
    for (let i =1 ; i<=21;i++){
      const fecha7DiasAntes = new Date();
      const fecha14DiasAntes = new Date();
      const fecha21DiasAntes = new Date();
      if (i<=7){
        
        fecha7DiasAntes.setDate(fechaActual.getDate() - i);
        primero.unshift(this.formatDate(fecha7DiasAntes))
      }else if(i<=14 && i>7){
        fecha14DiasAntes.setDate(fechaActual.getDate() - i);
        segundo.unshift(this.formatDate(fecha14DiasAntes))
        
      }else if(i>14 && i<=21){
        fecha21DiasAntes.setDate(fechaActual.getDate() - i);
        tercero.unshift(this.formatDate(fecha21DiasAntes))
        
      }
      
    }
  
    if (dias === "7") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${primero.join("|")}).*`);
      console.log('7');
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      this.diaSeleccionado="Últimos 7 días"
      
    } else if (dias === "14") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${segundo.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('14');
      this.diaSeleccionado="Últimos 14 días"
    } else if (dias === "21") {
      this.dataSource.data=[...this.resp]
      const regex = new RegExp(`.*(${tercero.join("|")}).*`);
      this.dataSource.data = this.dataSource.data.filter(elemento => regex.test(elemento.fecha_envio) );
      console.log('21');
      this.diaSeleccionado="Últimos 21 días"
    } else if (dias === "todos"){
      this.dataSource.data = [...this.resp]
      this.diaSeleccionado="Todas las fechas"
    }
  }

getSelectedRows() {

  this.dataSource.data.forEach((element) => {
    if (this.selection.isSelected(element)) {
      this.svRecibidos.tramitesEnvioBloque.push(element);
    }
  });

  console.log(this.svRecibidos.tramitesEnvioBloque)
}
openBloque(){
  this.svRecibidos.tramitesEnvioBloque= []
  this.getSelectedRows()
  const dialogRef = this.dialog.open(EnviarTodosComponent);

  dialogRef.afterClosed().subscribe(result => {
    if(this.svRecibidos.controlBloque==true){
      this.prueba.tramiteN.unshift(...this.svRecibidos.tramitesEnvioBloque)
        this.dataSource.data = this.dataSource.data.filter((elemento) => !this.svRecibidos.tramitesEnvioBloque.includes(elemento));
        const currentUrl = this.router.url;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);
        this.cargarTramite();
      }
      this.svRecibidos.controlBloque=false;
      this.svRecibidos.enviado=true;
      this.cargarTramite();
      
    })
}
contador: number = 0;

actualizarContador(checked: boolean) {
  if (checked) {
    this.contador++;
  }else {
    this.contador--;
  }
}

resetCont(checked: boolean){
  if(checked){
    this.contador = 2;
  }else{
    this.contador = 0;
  }
}

}



@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html',
})
export class crearComponent {}


