import { Component } from '@angular/core';
import {  MatDialog, MatDialogRef } from '@angular/material/dialog';
import { BuscarPersonaComponent } from '../../buscar-persona/buscar-persona.component';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TramiteModule } from '../../tramite.module';
import { Router } from '@angular/router';
@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.component.html',
  styleUrls: ['./enviar.component.scss']
})
export class EnviarComponentRev {
  constructor(public buscarPersona: MatDialog, public fusionar: MatDialog, private _snackBar: MatSnackBar, public control : TramiteModule,private router: Router,public dialogRef: MatDialogRef<EnviarComponentRev>){}

  buttonLabel: string = 'Acción 1'; // Etiqueta del botón
  isButtonClicked: boolean = false; // Variable de control

  buttonClicked(): void {
    if (this.isButtonClicked) {
      // Acción para el segundo clic
      console.log('Segundo clic');
      this.control.control=true;
      // Realiza la lógica deseada para el segundo clic

      this.dialogRef.close();
      // const currentUrl = this.router.url;
      // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      // this.router.onSameUrlNavigation = 'reload';
      // this.router.navigate([currentUrl]);
      //   this._snackBar.open('Tramite Enviado Exitosamente','',{
      //     duration:1500,
      //     horizontalPosition:'center',
      //     verticalPosition:'bottom'
      //     });

      this.buttonLabel = 'Acción 1'; // Restablece la etiqueta del botón para el siguiente primer clic
    } else {
      // Acción para el primer clic
      console.log('Primer clic');
      // Realiza la lógica deseada para el primer clic
      this._snackBar.open('Cambios Guardados','',{duration: 3000,});
      this.buttonLabel = 'Acción 2'; // Cambia la etiqueta del botón para indicar la próxima acción en el segundo clic
      this.isButtonClicked = true; // Actualiza el estado del botón para indicar que el primer clic ya ocurrió
    }
  }

  openBuscarPersona(){

    const dialogRef = this.buscarPersona.open(BuscarPersonaComponent);

      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }



  de = [
    {
      name: 'Oscar Martinez Ramos',
      dep: 'SEM',
      car: 'Asistente Legal',
      icon: 'account_circle'
    }
  ];

  para = [
    {
      name: 'leslie.santiago',
      dep: 'UNIDAD DE PROCESOS JURIDICCIONALES ',
      car: 'ASISTENTE TECNICO UPJ DAJ',
      icon: 'account_circle'
    }
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.de.push({name: value,dep: value,car: value, icon: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  add2(event2: MatChipInputEvent): void {
    const value = (event2.value || '').trim();

    // Add our fruit
    if (value) {
      this.para.push({name: value,dep: value,car: value,icon:value});
    }

    // Clear the input value
    event2.chipInput!.clear();
  }

  remove(des: any): void {
    const index = this.de.indexOf(des);


    if (index >= 0) {
      this.de.splice(index, 1);

    }
  }

  remove2(paras: any): void {

    const index = this.para.indexOf(paras);

    if (index >= 0) {

      this.para.splice(index, 1);
    }
  }


}
