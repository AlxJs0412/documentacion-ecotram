import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardEtrComponent } from './card-etr.component';

describe('CardEtrComponent', () => {
  let component: CardEtrComponent;
  let fixture: ComponentFixture<CardEtrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardEtrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardEtrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
