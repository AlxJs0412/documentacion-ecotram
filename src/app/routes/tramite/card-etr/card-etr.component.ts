import { Component } from '@angular/core';

@Component({
  selector: 'app-card-etr',
  templateUrl: './card-etr.component.html',
  styleUrls: ['./card-etr.component.scss']
})
export class CardEtrComponent {
  stats = [
    {
      title: 'Entrante',
      amount: '12',
      color: 'bg-red-400',
      //icon1: '/assets/images/iconEcotram/advertencia.png',
      icon: 'warning',
      text: ' #FF3364' ,
      estilo: '65px' 
    },
    {
      title: 'Recibidos',
      amount: '28',
      color: 'bg-yellow-500',
      //icon1: '/assets/images/iconEcotram/verificar.png',
      icon: 'check_circle',
      text: '#ffc400',
      estilo: '65px'
    },
    {
      title: 'Revertidos',
      amount: '4',
      color: 'bg-green-500',
      //icon1: '/assets/images/iconEcotram/atras.png',
      icon: 'subdirectory_arrow_left',
      text: '#ffc400',
      estilo: '65px'
    },
    {
      title: 'Enviados',
      amount: '9',
      color: 'bg-teal-500',
      //icon1: '/assets/images/iconEcotram/enviar.png',
      icon: 'send',
      text: '#48C87B',
      estilo: '65px'
    },
  ];
}
