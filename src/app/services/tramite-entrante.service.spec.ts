import { TestBed } from '@angular/core/testing';

import { TramiteEntranteService } from './tramite-entrante.service';

describe('TramiteEntranteService', () => {
  let service: TramiteEntranteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TramiteEntranteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
