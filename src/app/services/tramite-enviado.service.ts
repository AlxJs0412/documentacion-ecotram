import { Injectable } from '@angular/core';
import { tramite } from 'app/routes/tramite/tramites-enviados/tramites-enviados.component';

@Injectable({
  providedIn: 'root'
})
export class TramiteEnviadoService {

  constructor() { }

  lisTramite: tramite[] = [
    {nro_tramite:8881,nro_copia:'ORIGINAL',asunto: 'PRUEBA - REMISION DE FACTURAS',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-04-12 11:20:43',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:8882,nro_copia:'ORIGINAL',asunto: 'PLAN DE ORDENAMIENTO VEHICULAR',tipo_tramite:'ORDEN DE DESPACHO',procedencia:'INTERNO',fecha_envio:'2023-04-06 12:45:08',tomado_por:'maritzabel.rosso',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:8883,nro_copia:'ORIGINAL',asunto: 'ASTURIZAGA RODAS RAFAEL - BAJA DE VEHICULO',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'	2022-10-21 17:50:56',tomado_por:'lizeth.marin',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:8884,nro_copia:'1',asunto: 'TICONA MARZA MARIANA - ALTA PARA PAGO DE PATENTE Y CAMBIO DE NOMBRE',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-03-23 15:22:39',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:8885,nro_copia:'ORIGINAL',asunto: '	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2022-03-04 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'},
    {nro_tramite:8886,nro_copia:'ORIGINAL',asunto: '	ENBVIO DE PRUEBA - SOLICITUD',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2022-03-05 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'}
  ];


  getTramite(){
    return this.lisTramite.slice();
  }
  eliminarTramite(index:number){
    this.lisTramite.splice(index,1);
  }

  agregarTramite(tramites: tramite){
   this.lisTramite.unshift(tramites);
  }


}
