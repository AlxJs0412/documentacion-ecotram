import { Injectable } from '@angular/core';
import { tramite } from 'app/routes/tramite/tramites-entrantes/tramites-entrantes.component';

@Injectable({
  providedIn: 'root'
})
export class TramiteEntranteService {
  lisTramite: tramite[] = [
    {nro_tramite:4441,nro_copia:'ORIGINAL',asunto: 'PRUEBA - REMISION DE FACTURAS',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-04-12 11:20:43',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:4442,nro_copia:'ORIGINAL',asunto: 'PLAN DE ORDENAMIENTO VEHICULAR',tipo_tramite:'ORDEN DE DESPACHO',procedencia:'INTERNO',fecha_envio:'2023-04-06 12:45:08',tomado_por:'maritzabel.rosso',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:4443,nro_copia:'ORIGINAL',asunto: 'ASTURIZAGA RODAS RAFAEL - BAJA DE VEHICULO',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'	2022-10-21 17:50:56',tomado_por:'lizeth.marin',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:4444,nro_copia:'1',asunto: 'TICONA MARZA MARIANA - ALTA PARA PAGO DE PATENTE Y CAMBIO DE NOMBRE',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-03-23 15:22:39',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:4445,nro_copia:'ORIGINAL',asunto: '	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2022-03-04 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'},
    {nro_tramite:4446,nro_copia:'ORIGINAL',asunto: '	ENBVIO DE PRUEBA - SOLICITUD',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2022-03-05 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'}
  ];

  constructor() { }
  getTramite(){
    return this.lisTramite.slice();
  }
  eliminarTramite(index:number){
    this.lisTramite.splice(index,1);
  }

  agregarTramite(tramites: tramite){
   this.lisTramite.unshift(tramites);
  }
}
