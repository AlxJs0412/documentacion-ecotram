import { TestBed } from '@angular/core/testing';

import { TramiteRecibidoService } from './tramite-recibido.service';

describe('TramiteRecibidoService', () => {
  let service: TramiteRecibidoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TramiteRecibidoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
