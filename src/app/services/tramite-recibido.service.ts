import { Injectable } from '@angular/core';
import { tramite, TramitesRecibidosComponent } from 'app/routes/tramite/tramites-recibidos/tramites-recibidos.component';

@Injectable({
  providedIn: 'root'
})
export class TramiteRecibidoService {
  constructor(){}
  filtrado: string=""

  // borrador/creados
  borrador : boolean=false
  datosBorrador : any []=[]
  asuntoBorrador : string =""

  //fin  borrador/creados



  //control cambio de pestañas
  entrante: boolean = false
  recibido: boolean = false
  revertido: boolean = false
  creado: boolean = false
  enviado: boolean = false

  //fin control cambio pestañas




  cargardatos(){
    console.log('a')
  }
  tramitesEnvioBloque : tramite [] = []

  //control envio bloque 
  controlBloque: boolean = false

  //persona buscar
  persona: any [] = []

  personasCopia : any [] = []

  tramiteSeleccionado : tramite [] = []

  seleccionarTramite = () => {

  }

  //materiales fusionar
  nodoPadre: tramite [] = []
  nodoHijo: tramite [] = []
  other: tramite[]=[]

  datosTramitePadre = (element: tramite ) => {
    this.nodoPadre=[];
    this.nodoPadre.unshift(element)
    console.log ('datosTramitePadre' + this.nodoPadre)
    this.datosTramiteHijo()
  }

  datosTramiteHijo = () => {
    this.nodoHijo = this.lisTramite.filter(element=>!this.nodoPadre.includes(element));
    console.log ('datosTramiteHijo' + this.nodoHijo)
  }

  datosFusionar = (  padreOrHijo : boolean ) => {  }

  actualizarDatosRecibidos = () => {

    console.log(this.tramitesRecibidos)
  }

  //fin materiales fusionar

  tramitesRecibidos : tramite [][]=[
    [ {nro_tramite:7771,nro_copia:'ORIGINAL',asunto: 'GAMLP - REQUERIMIENTO DE PAPEL MEMBRETADO',tipo_tramite:'	CORRESPONDENCIA EJECUTIVO MUNICIPAL',procedencia:'INTERNO',fecha_envio:'	2023-06-12 11:41:06',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Urgente'} ],
    [ {nro_tramite:7772,nro_copia:'ORIGINAL',asunto: '	SOLICITUD DE LICENCIA DE FUNCIONAMIENTO',tipo_tramite:'ORDEN DE DESPACHO',procedencia:'INTERNO',fecha_envio:'2023-06-10 17:47:51',tomado_por:'maritzabel.rosso',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'}, ],
    [ {nro_tramite:7773,nro_copia:'ORIGINAL',asunto: '	LLANOS PEDRO - ACLATRACION SOL DE APERTURA DE GARAJE',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-06-07 14:30:13',tomado_por:'lizeth.marin',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'}, ],
    [ {nro_tramite:7774,nro_copia:'1',asunto: 'GAMLP/SMDE/ N. 019/23. - CRONOGRAMA DE OPERATIVOS USO DE MOVILIDADES',tipo_tramite:'	CORRESPONDENCIA EJECUTIVO MUNICIPAL',procedencia:'EXTERNO',fecha_envio:'2023-06-03 14:51:03',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'}, ],
    [ {nro_tramite:7775,nro_copia:'ORIGINAL',asunto: '	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-05-31 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'}, ],
    [ {nro_tramite:7776,nro_copia:'ORIGINAL',asunto: 'ASOCIACION DE COMERCIANTES MINORISTAS EN TELAS - AUDIENCIA',tipo_tramite:'	COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS',procedencia:'EXTERNO',fecha_envio:'2023-05-26 15:09:54',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'} ],
  ]
  
  datosTramiteRecibidos = () => {
    console.log('datos primer')
    this.lisTramite = this.tramitesRecibidos.map(subarray => subarray[0]);
    console.log(this.lisTramite)
  }

  lisTramite: tramite[] = this.tramitesRecibidos.map(subarray => subarray[0]);

  // lisTramite: tramite[] = [
  //   {nro_tramite:7771,nro_copia:'ORIGINAL',asunto: 'GAMLP - REQUERIMIENTO DE PAPEL MEMBRETADO',tipo_tramite:'	CORRESPONDENCIA EJECUTIVO MUNICIPAL',procedencia:'INTERNO',fecha_envio:'	2023-04-11 11:41:06',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Urgente'},
  //   {nro_tramite:7772,nro_copia:'ORIGINAL',asunto: '	SOLICITUD DE LICENCIA DE FUNCIONAMIENTO',tipo_tramite:'ORDEN DE DESPACHO',procedencia:'INTERNO',fecha_envio:'2023-04-06 17:47:51',tomado_por:'maritzabel.rosso',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
  //   {nro_tramite:7773,nro_copia:'ORIGINAL',asunto: '	LLANOS PEDRO - ACLATRACION SOL DE APERTURA DE GARAJE',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-04-06 14:30:13',tomado_por:'lizeth.marin',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
  //   {nro_tramite:7774,nro_copia:'1',asunto: 'GAMLP/SMDE/ N. 019/23. - CRONOGRAMA DE OPERATIVOS USO DE MOVILIDADES',tipo_tramite:'	CORRESPONDENCIA EJECUTIVO MUNICIPAL',procedencia:'EXTERNO',fecha_envio:'2023-03-30 14:51:03',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
  //   {nro_tramite:7775,nro_copia:'ORIGINAL',asunto: '	JUAN PEREZ PEREZ - APROBACION FRACCIONAMIENTO FUERA DE NORMA',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2022-03-04 09:09:56',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'},
  //   {nro_tramite:7776,nro_copia:'ORIGINAL',asunto: 'ASOCIACION DE COMERCIANTES MINORISTAS EN TELAS - AUDIENCIA',tipo_tramite:'	COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS',procedencia:'EXTERNO',fecha_envio:'2023-03-27 15:09:54',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Media'}
  // ];


  getTramite(){
    return this.lisTramite.slice();
  }
  eliminarTramite(index:number){
    this.lisTramite.splice(index,1);
  }

  agregarTramite(tramites: tramite){
   this.lisTramite.unshift(tramites);
  }

}
