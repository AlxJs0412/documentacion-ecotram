import { TestBed } from '@angular/core/testing';

import { TramiteEnviadoService } from './tramite-enviado.service';

describe('TramiteEnviadoService', () => {
  let service: TramiteEnviadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TramiteEnviadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
