import { TestBed } from '@angular/core/testing';

import { TramiteRevertidoService } from './tramite-revertido.service';

describe('TramiteRevertidoService', () => {
  let service: TramiteRevertidoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TramiteRevertidoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
