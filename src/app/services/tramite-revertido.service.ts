import { Injectable } from '@angular/core';
import { tramite } from 'app/routes/tramite/tramites-revertidos/tramites-revertidos.component';

@Injectable({
  providedIn: 'root'
})
export class TramiteRevertidoService {
  lisTramite: tramite[] = [
    {nro_tramite:5551,nro_copia:'ORIGINAL',asunto: 'ALVARADO FORONDA ALEXANDER - CAMBIO DE NOMBRE',tipo_tramite:'	COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS',procedencia:'EXTERNO',fecha_envio:'2023-04-18 00:00:14',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:5552,nro_copia:'ORIGINAL',asunto: 'FEDERACION DEPARTAMENTAL DE GREMIALES ARTESANOS LA PAZ - SOLICITUD DE REVISION DE RESOLUCION ADMINISTRATIVA Nº 216/2020',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'INTERNO',fecha_envio:'2023-04-18 00:00:14',tomado_por:'maritzabel.rosso',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:5553,nro_copia:'ORIGINAL',asunto: 'ESPINOZA GUACHALLA BEATRIZ - CAMBIO DE NOMBRE',tipo_tramite:'COMERCIO EN VÍAS Y ESPACIOS PÚBLICOS',procedencia:'EXTERNO',fecha_envio:'2023-04-18 00:00:14',tomado_por:'lizeth.marin',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Baja'},
    {nro_tramite:5554,nro_copia:'ORIGINAL',asunto: 'JIMENEZ MENDOZA ALBERTO - APELACION',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-04-18 00:00:14',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'3',prioridad:'Urgente'},
    {nro_tramite:5555,nro_copia:'ORIGINAL',asunto: 'BARROSO MENDEZ JUAN PABLO - REPRESENTACIÓN MEMORÁNDUM D.G.RR.HH 01653/2023',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-03-23 00:02:01',tomado_por:'m.tellez',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Media'},
    {nro_tramite:5556,nro_copia:'ORIGINAL',asunto: 'QUISPE VEGA SILVIA EUGENIA - VIOLACIÓN DE DERECHOS ADQUIRIDOS INAMOVILIDAD FUNCIONARIA',tipo_tramite:'CORRESPONDENCIA CIUDADANA',procedencia:'EXTERNO',fecha_envio:'2023-02-16 00:02:01',tomado_por:'',so_sistema_nombre:'ECOTRAM',dias_atencion:'',prioridad:'Media'}
  ];
  constructor() { }
  getTramite(){
    return this.lisTramite.slice();
  }
  eliminarTramite(index:number){
    this.lisTramite.splice(index,1);
  }

  agregarTramite(tramites: tramite){
   this.lisTramite.unshift(tramites);
  }
}
