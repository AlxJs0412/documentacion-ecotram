import { Component } from '@angular/core';

@Component({
  selector: 'app-notification',
  template: `
    <button mat-icon-button [matMenuTriggerFor]="menu">
      <mat-icon matBadge="5" matBadgeColor="warn">notifications</mat-icon>
    </button>

    <mat-menu #menu="matMenu" >


      <div style="background-color: #757fef; display: flex; align-items: center; padding:10%;justify-content: center; margin-bottom: 10px; flex-direction: row-reverse;">
        <mat-icon style="margin-left: auto;">keyboard_arrow_right</mat-icon>
        <span style="margin-left: 5; ">Notificaciones</span>
      </div>

      <div style=" display: flex; align-items: center; padding:2%;justify-content: center; margin-bottom: 10px; flex-direction: row-reverse;">
        <mat-icon style="margin-left: auto;">close</mat-icon>
        <span style="margin-left: auto;">comunicados1</span>
      </div>
      <div style="display: flex; align-items: center; justify-content: center; margin-bottom: 10px; flex-direction: row-reverse;">
        <mat-icon style="margin-left: 15px;">close</mat-icon>
        <span style="margin-left: 5px;">comunicados1</span>
      </div>
    </mat-menu>
  `,
})
export class NotificationComponent {
  messages = ['Notificaciones'];
}
